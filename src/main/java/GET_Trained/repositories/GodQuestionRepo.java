package GET_Trained.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import GET_Trained.domain.GodQuestion;

@Repository
public interface GodQuestionRepo extends JpaRepository<GodQuestion, Integer> {
	GodQuestion findOne(int id);

}
