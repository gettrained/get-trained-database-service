package GET_Trained.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import GET_Trained.domain.GmdTutorialtype;

@Repository
public interface GmdTutorialtypeRepo extends JpaRepository<GmdTutorialtype, Integer> {

	GmdTutorialtype findOne(Integer id);
}
