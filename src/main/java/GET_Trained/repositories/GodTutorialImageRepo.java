package GET_Trained.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import GET_Trained.domain.GodTutorialimage;

@Repository
public interface GodTutorialImageRepo extends JpaRepository<GodTutorialimage, Integer> {
	GodTutorialimage findOne(Integer id);
}
