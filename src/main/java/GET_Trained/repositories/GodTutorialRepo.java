package GET_Trained.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import GET_Trained.domain.GodTutorial;

@Repository
public interface GodTutorialRepo extends JpaRepository<GodTutorial, Integer> {
	GodTutorial findOne(int id);
	
	@Query("Select t from GodTutorial t WHERE t.identificationcode = :id")
	GodTutorial findByIDCode(@Param("id") int id);
}
