package GET_Trained.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import GET_Trained.domain.GodAnswer;

@Repository
public interface GodAnswerRepo extends JpaRepository<GodAnswer, Integer> {

	GodAnswer findOne(Integer id);
	
	
}
