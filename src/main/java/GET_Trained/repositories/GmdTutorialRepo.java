package GET_Trained.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import GET_Trained.domain.GmdTutorial;

@Repository
public interface GmdTutorialRepo extends JpaRepository<GmdTutorial, Integer> {
	GmdTutorial findOne(int id);
	
	@Query("Select t from GmdTutorial t WHERE t.identificationcode = :id")
	GmdTutorial findByIDCode(@Param("id") int id);
}
