package GET_Trained.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import GET_Trained.domain.GmdQuestion;

@Repository
public interface GmdQuestionRepo extends JpaRepository<GmdQuestion, Integer> {
	GmdQuestion findOne(Integer id);
}
