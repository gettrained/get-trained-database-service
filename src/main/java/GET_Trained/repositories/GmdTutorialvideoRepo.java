package GET_Trained.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import GET_Trained.domain.GmdTutorialvideo;

@Repository
public interface GmdTutorialvideoRepo extends JpaRepository<GmdTutorialvideo, Integer> {

	GmdTutorialvideo findOne(Integer id);
}
