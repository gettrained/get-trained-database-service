package GET_Trained.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import GET_Trained.domain.GodPersonalanswer;

@Repository
public interface GodPersonalanswerRepo extends JpaRepository<GodPersonalanswer, Integer> {
	GodPersonalanswer findOne(int id);

	@Query("Select t from GodPersonalanswer t WHERE t.godPersonaltutorial.id = :tutid AND t.godAnswer.id = :ansid ")
	GodPersonalanswer findByPerstutandAnswerID(@Param("tutid") int tutid, @Param("ansid") int ansid);

	
}
