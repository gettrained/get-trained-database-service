package GET_Trained.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import GET_Trained.domain.GmdTutorialmaterial;

@Repository
public interface GmdTutorialmaterialRepo extends JpaRepository<GmdTutorialmaterial, Integer> {

	GmdTutorialmaterial findOne(Integer id);
}
