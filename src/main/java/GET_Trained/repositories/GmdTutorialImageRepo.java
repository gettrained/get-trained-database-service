package GET_Trained.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import GET_Trained.domain.GmdTutorialimage;

@Repository
public interface GmdTutorialImageRepo extends JpaRepository<GmdTutorialimage, Integer> {
	GmdTutorialimage findOne(Integer id);
}
