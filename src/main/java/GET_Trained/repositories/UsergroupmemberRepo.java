package GET_Trained.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import GET_Trained.domain.Usergroupmember;

@Repository
public interface UsergroupmemberRepo extends JpaRepository<Usergroupmember, Integer> {
	Usergroupmember findOne(int id);
	
	
}
