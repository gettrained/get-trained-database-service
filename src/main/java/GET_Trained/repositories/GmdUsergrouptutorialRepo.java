package GET_Trained.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import GET_Trained.domain.GmdUsergrouptutorial;

@Repository
public interface GmdUsergrouptutorialRepo extends JpaRepository<GmdUsergrouptutorial, Integer> {

	GmdUsergrouptutorial findOne(Integer id);
}
