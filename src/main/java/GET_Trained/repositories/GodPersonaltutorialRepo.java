package GET_Trained.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import GET_Trained.domain.GodPersonaltutorial;

@Repository
public interface GodPersonaltutorialRepo extends JpaRepository<GodPersonaltutorial, Integer> {
	GodPersonaltutorial findOne(int id);

}
