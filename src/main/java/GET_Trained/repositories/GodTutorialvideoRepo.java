package GET_Trained.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import GET_Trained.domain.GodTutorialvideo;

@Repository
public interface GodTutorialvideoRepo extends JpaRepository<GodTutorialvideo, Integer> {
	GodTutorialvideo findOne(int id);
}
