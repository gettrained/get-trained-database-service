package GET_Trained.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import GET_Trained.domain.GodTutorialtype;

@Repository
public interface GodTutorialtypeRepo extends JpaRepository<GodTutorialtype, Integer> {
	GodTutorialtype findOne(int id);
	
	GodTutorialtype findBytype(String type); 
		
	
}
