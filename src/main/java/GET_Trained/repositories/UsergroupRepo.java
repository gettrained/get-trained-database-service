package GET_Trained.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import GET_Trained.domain.Usergroup;

@Repository
public interface UsergroupRepo extends JpaRepository<Usergroup, Integer> {
	Usergroup findOne(int id);
}
