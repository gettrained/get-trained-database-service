package GET_Trained;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.cloud.netflix.eureka.*;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan
@EnableEurekaClient
public class GetTrainedApplication  {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(GetTrainedApplication.class);
		app.run(args);

	}

}
