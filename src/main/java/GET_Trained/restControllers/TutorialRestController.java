package GET_Trained.restControllers;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Base64;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.lowagie.text.DocumentException;

import GET_Trained.Strings;
import GET_Trained.domain.Employee;
import GET_Trained.domain.GmdAnswer;
import GET_Trained.domain.GmdQuestion;
import GET_Trained.domain.GmdTutorial;
import GET_Trained.domain.GmdTutorialimage;
import GET_Trained.domain.GmdTutorialmaterial;
import GET_Trained.domain.GmdTutorialvideo;
import GET_Trained.domain.GodAnswer;
import GET_Trained.domain.GodPersonalanswer;
import GET_Trained.domain.GodPersonaltutorial;
import GET_Trained.domain.GodQuestion;
import GET_Trained.domain.GodTutorial;
import GET_Trained.domain.GodTutorialimage;
import GET_Trained.domain.GodTutorialmaterial;
import GET_Trained.domain.GodTutorialtype;
import GET_Trained.domain.GodTutorialvideo;
import GET_Trained.domain.Usergroup;
import GET_Trained.repositories.EmployeeRepo;
import GET_Trained.repositories.GmdTutorialRepo;
import GET_Trained.repositories.GodAnswerRepo;
import GET_Trained.repositories.GodPersonalanswerRepo;
import GET_Trained.repositories.GodPersonaltutorialRepo;
import GET_Trained.repositories.GodQuestionRepo;
import GET_Trained.repositories.GodTutorialImageRepo;
import GET_Trained.repositories.GodTutorialRepo;
import GET_Trained.repositories.GodTutorialmaterialRepo;
import GET_Trained.repositories.GodTutorialtypeRepo;
import GET_Trained.repositories.GodTutorialvideoRepo;
import GET_Trained.transferclasses.PostFinishPersonalAnswer;
import GET_Trained.transferclasses.PostPausePersonalAnswer;
import GET_Trained.transferclasses.PostPersonalAnswer;
import GET_Trained.transferclasses.PostPersonalAnswers;
import GET_Trained.transferclasses.PostPersonalTutorial;
import GET_Trained.transferclasses.PostResult;
import GET_Trained.transferclasses.ToAnswer;
import GET_Trained.transferclasses.ToEmployee;
import GET_Trained.transferclasses.ToPersTutorial;
import GET_Trained.transferclasses.ToPersTutorials;
import GET_Trained.transferclasses.ToPersonalAnswer;
import GET_Trained.transferclasses.ToPersonalAnswers;
import GET_Trained.transferclasses.ToPersonalTutorialResult;
import GET_Trained.transferclasses.ToQuestion;
import GET_Trained.transferclasses.ToQuestions;
import GET_Trained.transferclasses.ToTutorial;
import GET_Trained.transferclasses.ToTutorialImage;
import GET_Trained.transferclasses.ToTutorialMaterial;
import GET_Trained.transferclasses.ToTutorialMaterials;
import GET_Trained.transferclasses.ToTutorialVideo;
import GET_Trained.transferclasses.ToTutorials;

@RestController
@EnableWebMvc
public class TutorialRestController {

	@Autowired
	private EmployeeRepo employeeRepo;
	@Autowired
	private GodTutorialRepo godTutorialRepo;
	@Autowired
	private GodPersonaltutorialRepo godPersonaltutorialRepo;
	@Autowired
	private GmdTutorialRepo gmdTutorialRepo;
	@Autowired
	private GodQuestionRepo godQuestionRepo;
	@Autowired
	private GodAnswerRepo godAnswerRepo;
	@Autowired
	private GodTutorialmaterialRepo godTutorialmaterialRepo;
	@Autowired
	private GodTutorialImageRepo godTutorialImageRepo;
	@Autowired
	private GodTutorialtypeRepo godTutorialtypeRepo;
	@Autowired
	private GodTutorialvideoRepo godTutorialvideoRepo;
	@Autowired
	private GodPersonalanswerRepo godPersonalanswerRepo;
	@Autowired
	private TemplateEngine templateEngine;

	/*
	 * Returns a List (in ToTutoials) of Tutorials (ToTutorial) of the employee
	 * specified by the given Id. If the id belongs to no employee the status code
	 * No Content (204) is returned. Checks if the Tutorial is already copied over
	 * to God_Tutorial, if not it gets copied over
	 */
	@GetMapping(value = "/employee/getTutorials/{id}", produces = "application/json")
	public ResponseEntity<ToTutorials> getTutorials(@PathVariable int id) {
		Employee employee = employeeRepo.findOne(id);
		if (employee == null) {
			// No Employee with specified Id found.
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
		}
		Set<Usergroup> usergroups = employee.getUsergroups();
		LinkedList<ToTutorial> tutorials = new LinkedList<>();
		for (Usergroup usergroup : usergroups) {
			Set<GmdTutorial> gmdtutorials = usergroup.getGmdtutorials();

			for (GmdTutorial gmdTutorial : gmdtutorials) {
				GodTutorial godTutorial = godTutorialRepo.findByIDCode(gmdTutorial.getIdentificationcode());

				if (godTutorial == null) {
					// Tutorial was not yet copied to God_Tutorial. Fetching it from Gmd_Tutorial
					godTutorial = godTutorialRepo.save(new GodTutorial(gmdTutorial.getColor(), //
							gmdTutorial.getIdentificationcode(), //
							gmdTutorial.getSuccesspercentage(), gmdTutorial.getTutorialname(), //
							gmdTutorial.getValid(), //
							gmdTutorial.getVersion(), //
							null, null));

					tutorials.add(new ToTutorial(godTutorial.getId(), //
							godTutorial.getSuccesspercentage(), //
							godTutorial.getColor(), //
							godTutorial.getTutorialname()));
				} else {
					// Tutorial is already in God_Tutorial
					tutorials.add(new ToTutorial(godTutorial.getId(), //
							godTutorial.getSuccesspercentage(), //
							godTutorial.getColor(), //
							godTutorial.getTutorialname()));
				}
			}
		}
		// Sorting to maintain a constant order even if the Repo mixes the Tutorials
		tutorials.sort(new Comparator<ToTutorial>() {

			@Override
			public int compare(ToTutorial o1, ToTutorial o2) {
				if (o1.getId() < o2.getId()) {
					return 1;
				}
				if (o1.getId() > o2.getId()) {
					return -1;
				}
				return 0;
			}
		});

		return ResponseEntity.status(HttpStatus.OK).body(new ToTutorials(tutorials));
	}

	/*
	 * Returns a List (in ToPersTutorials) of Personal Tutorials (ToPersTutorials)
	 * of the employee specified by the given Id. If the id belongs to no employee
	 * the status code No Content (204) is returned.
	 */
	@GetMapping(value = "/employee/getPersTutorials/{id}", produces = "application/json")
	public ResponseEntity<ToPersTutorials> getPersTutorials(@PathVariable int id) {
		Employee employee = employeeRepo.findOne(id);
		// No Employee with specified Id found.
		if (employee == null) {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
		}
		Set<GodPersonaltutorial> personaltutorials = employee.getGodPersonaltutorials();
		LinkedList<ToPersTutorial> tutorials = new LinkedList<>();
		for (GodPersonaltutorial godPersonaltutorial : personaltutorials) {
			tutorials.add(new ToPersTutorial(godPersonaltutorial.getId(), //
					godPersonaltutorial.getGodTutorial().getId(), //
					godPersonaltutorial.getState(), godPersonaltutorial.getLastseen(), //
					godPersonaltutorial.getDatefinished(), godPersonaltutorial.getDatecreated(), //
					godPersonaltutorial.getUsedtime()));
		}

		// Sorting to maintain a constant order even if the Repo mixes the PersTutorials
		tutorials.sort(new Comparator<ToPersTutorial>() {

			@Override
			public int compare(ToPersTutorial o1, ToPersTutorial o2) {

				try {
					return (o1.getDatefinished().compareTo(o2.getDatefinished()) * (-1));
				} catch (NullPointerException e) {
					return 0;
				}

			}
		});

		return ResponseEntity.status(HttpStatus.OK).body(new ToPersTutorials(tutorials));
	}

	/*
	 * Returns the Tutorial (ToTutorial) specified by the given Id. If the id
	 * belongs to no GodTutorial the status code No Content (204) is returned.
	 */
	@GetMapping(value = "/getTutorial/{id}", produces = "application/json")
	public ResponseEntity<ToTutorial> getTutorial(@PathVariable int id) {
		GodTutorial godTutorial = godTutorialRepo.findOne(id);
		if (godTutorial == null) {
			// No GodTutorial with specified Id found.
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
		}

		return ResponseEntity.status(HttpStatus.OK).body(new ToTutorial(godTutorial.getId(), //
				godTutorial.getSuccesspercentage(), //
				godTutorial.getColor(), //
				godTutorial.getTutorialname()));
	}

	/*
	 * Returns the Employee (ToEmployee) specified by the given Id. If the id
	 * belongs to no Employee the status code No Content (204) is returned.
	 */
	@GetMapping(value = "/getEmployee/{id}", produces = "application/json")
	public ResponseEntity<ToEmployee> getEmployee(@PathVariable int id) {
		Employee employee = employeeRepo.findOne(id);
		if (employee == null) {
			// No Employee with specified Id found.
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
		}
		return ResponseEntity.status(HttpStatus.OK).body(new ToEmployee(employee.getId(), //
				employee.getFirstname(), //
				employee.getLastname()));
	}

	/*
	 * Returns the Employee (ToEmployee) specified by the given username. If the
	 * username belongs to no Employee the status code No Content (204) is returned.
	 */
	@GetMapping(value = "/getEmployeeByUser/{user}", produces = "application/json")
	public ResponseEntity<ToEmployee> getEmployeeByUser(@PathVariable String user) {
		Employee employee = employeeRepo.findBylogin(user);
		if (employee == null) {
			// No Employee with specified username found.
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
		}
		return ResponseEntity.status(HttpStatus.OK).body(new ToEmployee(employee.getId(), //
				employee.getFirstname(), //
				employee.getLastname()));
	}

	/*
	 * Returns a List (in ToQuestions) of Questions (ToQuestion) of the GodTutorial
	 * specified by the given Id. If the id belongs to no GodTutorial the status
	 * code No Content (204) is returned. Checks if the GodQuestion is already
	 * copied over to God_Question, if not it gets copied over.
	 */
	@GetMapping(value = "/tutorial/questions/{id}", produces = "application/json")
	public ResponseEntity<ToQuestions> getQuestions(@PathVariable int id) {
		GodTutorial godTutorial = godTutorialRepo.findOne(id);
		if (godTutorial == null) {
			// No GodTutorial with specified id found.
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
		}

		Set<GodQuestion> godQuestions = godTutorial.getGodQuestions();
		LinkedList<ToQuestion> questions = new LinkedList<>();

		if (godQuestions.isEmpty()) {
			// Question was not yet copied to God_Question. Fetching it from Gmd_Question
			GmdTutorial gmdTutorial = gmdTutorialRepo.findByIDCode(godTutorial.getIdentificationcode());

			Set<GmdQuestion> gmdQuestions = gmdTutorial.getGmdQuestions();
			for (GmdQuestion gmdQuestion : gmdQuestions) {
				LinkedList<ToAnswer> answers = new LinkedList<>();

				GodQuestion newGodQuestion = godQuestionRepo.save(new GodQuestion(gmdQuestion.getImage(),
						gmdQuestion.getText(), gmdQuestion.getOrder(), godTutorial, null));

				Set<GmdAnswer> gmdAnswers = gmdQuestion.getGmdAnswers();
				for (GmdAnswer gmdAnswer : gmdAnswers) {
					GodAnswer newGodAnswer = godAnswerRepo.save(new GodAnswer(gmdAnswer.getCorrect(), //
							gmdAnswer.getAnswertext(), //
							newGodQuestion));

					answers.add(new ToAnswer(newGodAnswer.getId(), //
							newGodAnswer.getCorrect(), //
							newGodAnswer.getAnswertext()));
				}
				try {
					final BufferedImage img = ImageIO.read(new ByteArrayInputStream(newGodQuestion.getImage()));
					final ByteArrayOutputStream baos = new ByteArrayOutputStream();
					ImageIO.write(img, "png", baos);
					final byte[] bytes = baos.toByteArray();

					questions.add(new ToQuestion(newGodQuestion.getId(), //
							bytes, //
							newGodQuestion.getText(), //
							newGodQuestion.getOrder(), //
							answers));
				} catch (Exception e) {
					questions.add(new ToQuestion(newGodQuestion.getId(), //
							null, //
							newGodQuestion.getText(), //
							newGodQuestion.getOrder(), //
							answers));
				}
			}
		} else {
			// Question was already copied to God_Question
			for (GodQuestion godQuestion : godQuestions) {
				LinkedList<ToAnswer> answers = new LinkedList<>();

				Set<GodAnswer> godAnswers = godQuestion.getGodAnswers();
				for (GodAnswer godAnswer : godAnswers) {
					answers.add(new ToAnswer(godAnswer.getId(), //
							godAnswer.getCorrect(), //
							godAnswer.getAnswertext()));
				}

				try {
					final BufferedImage img = ImageIO.read(new ByteArrayInputStream(godQuestion.getImage()));
					final ByteArrayOutputStream baos = new ByteArrayOutputStream();
					ImageIO.write(img, "png", baos);
					final byte[] bytes = baos.toByteArray();

					questions.add(new ToQuestion(godQuestion.getId(), //
							bytes, //
							godQuestion.getText(), //
							godQuestion.getOrder(), //
							answers));
				} catch (Exception e) {
					questions.add(new ToQuestion(godQuestion.getId(), //
							null, //
							godQuestion.getText(), //
							godQuestion.getOrder(), //
							answers));
				}
			}
		}
		return ResponseEntity.status(HttpStatus.OK).body(new ToQuestions(questions));
	}

	/*
	 * Returns a List (in ToTutorialMaterials) of Materials (ToTutorialMaterial) of
	 * the GodTutorial specified by the given Id. If the id belongs to no
	 * GodTutorial the status code No Content (204) is returned. Checks if the
	 * Tutorialmaterials are already copied over to God_Tutorialmaterial, if not
	 * they get copied over.
	 */
	@GetMapping(value = "/tutorial/material/{id}", produces = "application/json")
	public ResponseEntity<ToTutorialMaterials> getMaterials(@PathVariable int id) {
		GodTutorial godTutorial = godTutorialRepo.findOne(id);

		if (godTutorial == null) {
			// No GodTutorial with specified id found.
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
		}

		Set<GodTutorialmaterial> godTutorialmaterials = godTutorial.getGodTutorialmaterials();
		LinkedList<ToTutorialMaterial> materials = new LinkedList<>();

		if (godTutorialmaterials.isEmpty()) {
			// Tutorialmaterials were not yet copied to God_Tutorialmaterial. Fetching them
			// from Gmd_Tutorialmaterial
			GmdTutorial gmdTutorial = gmdTutorialRepo.findByIDCode(godTutorial.getIdentificationcode());

			Set<GmdTutorialmaterial> gmdTutorialmaterials = gmdTutorial.getGmdTutorialmaterials();
			for (GmdTutorialmaterial gmdTutorialmaterial : gmdTutorialmaterials) {

				GodTutorialtype godTutorialtype = godTutorialtypeRepo
						.findBytype(gmdTutorialmaterial.getGmdTutorialtype().getType());
				if (godTutorialtype == null) {
					godTutorialtype = godTutorialtypeRepo
							.save(new GodTutorialtype(gmdTutorialmaterial.getGmdTutorialtype().getType()));
				}
				GodTutorialmaterial godTutorialmaterial;

				GodTutorialimage godTutorialimage = null;
				GodTutorialvideo godTutorialvideo = null;

				if (gmdTutorialmaterial.getGmdTutorialtype().getType().toLowerCase().equals("image")) {
					GmdTutorialimage gmdTutorialimage = gmdTutorialmaterial.getGmdTutorialimage();
					godTutorialimage = godTutorialImageRepo
							.save(new GodTutorialimage(gmdTutorialimage.getImage(), gmdTutorialimage.getText()));
					godTutorialmaterial = godTutorialmaterialRepo.save(new GodTutorialmaterial(godTutorial,
							gmdTutorialmaterial.getOrder(), godTutorialtype, godTutorialimage));
				} else {
					GmdTutorialvideo gmdTutorialvideo = gmdTutorialmaterial.getGmdTutorialvideo();
					godTutorialvideo = godTutorialvideoRepo.save(new GodTutorialvideo(gmdTutorialvideo.getVideo()));
					godTutorialmaterial = godTutorialmaterialRepo.save(new GodTutorialmaterial(godTutorial,
							gmdTutorialmaterial.getOrder(), godTutorialtype, godTutorialvideo));
				}
				materials.add(new ToTutorialMaterial(godTutorialmaterial.getId(), godTutorial.getId(),
						godTutorialtype.getType(), godTutorialmaterial.getOrder()));
			}
		} else {
			// The Tutorialmaterials are already in God_Tutorialmaterial
			for (GodTutorialmaterial godTutorialmaterial : godTutorialmaterials) {
				if (godTutorialmaterial.getGodTutorialtype().getType().toLowerCase().equals("image")) {
					materials.add(new ToTutorialMaterial(godTutorialmaterial.getId(),
							godTutorialmaterial.getGodTutorial().getId(),
							godTutorialmaterial.getGodTutorialtype().getType(), godTutorialmaterial.getOrder()));
				} else {
					materials.add(new ToTutorialMaterial(godTutorialmaterial.getId(),
							godTutorialmaterial.getGodTutorial().getId(),
							godTutorialmaterial.getGodTutorialtype().getType(), godTutorialmaterial.getOrder()));
				}

			}
		}

		return ResponseEntity.status(HttpStatus.OK).body(new ToTutorialMaterials(materials));
	}

	/*
	 * Returns the Material Imgae (ToTutorialImage) of the GodTutorialMaterial
	 * specified by the given Id. If the id belongs to no GodTutorialMaterial or
	 * there is no godTutorialimage the status code No Content (204) is returned.
	 */
	@GetMapping(value = "/tutorial/material/image/{id}", produces = "application/json")
	public ResponseEntity<ToTutorialImage> getMaterialImage(@PathVariable int id) {
		GodTutorialmaterial godTutorialmaterial = godTutorialmaterialRepo.findOne(id);
		if (godTutorialmaterial == null)
			// No GodTutorialmaterial with specified id found.
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
		GodTutorialimage godTutorialimage = godTutorialmaterial.getGodTutorialimage();
		if (godTutorialimage == null)
			// No GodTutorialimage in current GodTutorialmaterial
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);

		try {
			final BufferedImage img = ImageIO.read(new ByteArrayInputStream(godTutorialimage.getImage()));
			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(img, "png", baos);
			final byte[] bytes = baos.toByteArray();
			return ResponseEntity.status(HttpStatus.OK).body(new ToTutorialImage(godTutorialimage.getText(), bytes));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.OK).body(new ToTutorialImage(godTutorialimage.getText(), null));
		}

	}

	/*
	 * Returns the Material Video (ToTutorialVideo) of the GodTutorialMaterial
	 * specified by the given Id. If the id belongs to no GodTutorialMaterial or
	 * there is no godTutorialvideo the status code No Content (204) is returned.
	 */
	@GetMapping(value = "/tutorial/material/video/{id}", produces = "application/json")
	public ResponseEntity<ToTutorialVideo> getMaterialVideo(@PathVariable int id) {
		GodTutorialmaterial godTutorialmaterial = godTutorialmaterialRepo.findOne(id);
		if (godTutorialmaterial == null)
			// No GodTutorialmaterial with specified id found.
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
		GodTutorialvideo godTutorialvideo = godTutorialmaterial.getGodTutorialvideo();
		if (godTutorialvideo == null)
			// No GodTutorialvideo found in current GodTutorialmaterial
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);

		return ResponseEntity.status(HttpStatus.OK).body(new ToTutorialVideo(godTutorialvideo.getVideo()));
	}

	/*
	 * Creates a new PersonalTutorial with the given values (in
	 * PostPersonalTutorial) and returns it (ToPersTutorial) If there is no employee
	 * or GodTutorial for given ids, the status code Not Modified (304) is returned.
	 */
	@PostMapping(value = "/employee/addpersonaltutorial", produces = "application/json")
	public ResponseEntity<ToPersTutorial> addPersonalTutorial(@RequestBody PostPersonalTutorial postPersonalTutorial) {

		if (employeeRepo.findOne(postPersonalTutorial.getEmployeeID()) == null
				|| godTutorialRepo.findOne(postPersonalTutorial.getTutorialID()) == null) {
			// No employee or GmdTutorial found with given ids.
			return ResponseEntity.status(HttpStatus.NOT_MODIFIED).body(null);
		}
		GodTutorial godTutorial = new GodTutorial();
		godTutorial.setId(postPersonalTutorial.getTutorialID());

		Employee employee = new Employee();
		employee.setId(postPersonalTutorial.getEmployeeID());

		GodPersonaltutorial godPersonaltutorial = godPersonaltutorialRepo
				.save(new GodPersonaltutorial(godTutorial, employee, "partial", "0", new Date(), 0));

		URI location = ServletUriComponentsBuilder.fromCurrentContextPath()//
				.path("/employee/getPersTutorials/{id}")//
				.buildAndExpand(godPersonaltutorial.getEmployee().getId()).toUri();

		return ResponseEntity.created(location).body(new ToPersTutorial(godPersonaltutorial.getId(), //
				godPersonaltutorial.getGodTutorial().getId(), //
				godPersonaltutorial.getState(), //
				godPersonaltutorial.getLastseen(), //
				godPersonaltutorial.getDatefinished(), //
				godPersonaltutorial.getDatecreated(), //
				godPersonaltutorial.getUsedtime()));
	}

	/*
	 * Returns a List (in ToPersonalAnswers) of Personalanswers (ToPersonalAnswer)
	 * of the GodPersonaltutorial specified by the given Id. If the id belongs to no
	 * GodPersonaltutorial the status code No Content (204) is returned.
	 */
	@GetMapping(value = "/employee/personalanswer/{id}", produces = "application/json")
	public ResponseEntity<ToPersonalAnswers> getPersonalAnswers(@PathVariable int id) {
		GodPersonaltutorial godPersonaltutorial = godPersonaltutorialRepo.findOne(id);
		LinkedList<ToPersonalAnswer> personalanswers = new LinkedList<>();
		if (godPersonaltutorial == null) {
			// No GodTutorialmaterial with specified id found.
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
		}
		Set<GodPersonalanswer> godPersonalanswers = godPersonaltutorial.getGodSelectedanswers();

		for (GodPersonalanswer godPersonalanswer : godPersonalanswers) {
			personalanswers.add(
					new ToPersonalAnswer(godPersonalanswer.getGodAnswer().getId(), godPersonalanswer.isSelected()));
		}

		return ResponseEntity.status(HttpStatus.OK).body(new ToPersonalAnswers(personalanswers));
	}

	/*
	 * Creates new GodPersonalanswers with the given values (in PostPersonalAnswers)
	 * If there are no personalAnswers in the Post Object (PostPersonalAnswers), no
	 * GodPersonaltutorial for given the id ,or no GodAnswer for given the id, the
	 * status code Not Modified (304) is returned.
	 */
	@PostMapping(value = "/employee/personaltutorial/addpersonalanswer", produces = "application/json")
	public ResponseEntity<PostResult> addPersonalAnswers(@RequestBody PostPersonalAnswers personalAnswers)
			throws URISyntaxException {
		if (personalAnswers.getPersonalanswers().isEmpty()) {
			// The given Post Object has no personalAnswers
			return ResponseEntity.status(HttpStatus.NOT_MODIFIED).body(new PostResult(false));
		}
		LinkedList<PostPersonalAnswer> answers = personalAnswers.getPersonalanswers();
		Set<GodPersonalanswer> checkedAnswers = new HashSet<>();
		for (PostPersonalAnswer item : answers) {
			GodPersonaltutorial godPersonaltutorial = godPersonaltutorialRepo.findOne(item.getPersonaltutorialID());
			if (godPersonaltutorial == null)
				// No GodPersonaltutorial with specified id found.
				return ResponseEntity.status(HttpStatus.NOT_MODIFIED).body(new PostResult(false));
			GodAnswer godAnswer = godAnswerRepo.findOne(item.getAnswerID());
			if (godAnswer == null)
				// No GodAnswer with specified id found.
				return ResponseEntity.status(HttpStatus.NOT_MODIFIED).body(new PostResult(false));

			// Checks if the Question is allready answered, if so it will get overridden so
			// the users can change their mind.
			GodPersonalanswer godPersonalanswer = godPersonalanswerRepo
					.findByPerstutandAnswerID(item.getPersonaltutorialID(), item.getAnswerID());
			if (godPersonalanswer != null) {
				if (godPersonalanswer.isSelected() != item.isSelected()) {
					godPersonalanswer.setSelected(item.isSelected());
					checkedAnswers.add(godPersonalanswer);
				}
			} else {
				checkedAnswers.add(new GodPersonalanswer(godPersonaltutorial, godAnswer, item.isSelected()));
			}

		}
		godPersonalanswerRepo.save(checkedAnswers);

		return ResponseEntity.created(new URI("")).body(null);
	}

	/*
	 * Modifies the specified Personaltutorial (id in PostFinishPersonalAnswer) to
	 * be finished. Returns Response Code 204 and a positive PostResult if
	 * successful. If there is no GodPersonaltutorial for given the id the status
	 * code Not Modified (304) is returned.
	 */
	@PostMapping(value = "employee/personaltutorial/finish", produces = "application/json")
	public ResponseEntity<PostResult> finishPersonalTutorial(
			@RequestBody PostFinishPersonalAnswer postFinishPersonalAnswer) {
		GodPersonaltutorial godPersonaltutorial = godPersonaltutorialRepo
				.findOne(postFinishPersonalAnswer.getPersonalTutorialID());
		if (godPersonaltutorial == null) {
			// No GodPersonaltutorial with specified id found.
			return ResponseEntity.status(HttpStatus.NOT_MODIFIED).body(new PostResult(false));
		}
		godPersonaltutorial.setDatefinished(new Date());
		godPersonaltutorial.setUsedtime(postFinishPersonalAnswer.getUsedtime());
		godPersonaltutorial.setState("done");
		godPersonaltutorialRepo.save(godPersonaltutorial);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(new PostResult(true));
	}

	/*
	 * Modifies the specified Personaltutorial (id in PostFinishPersonalAnswer) to
	 * be paused. Returns Response Code 204 and a positive PostResult if successful.
	 * If there is no GodPersonaltutorial for given the id the status code Not
	 * Modified (304) is returned.
	 */
	@PostMapping(value = "employee/personaltutorial/pause", produces = "application/json")
	public ResponseEntity<PostResult> pausePersonalTutorial(
			@RequestBody PostPausePersonalAnswer postPausePersonalAnswer) {
		GodPersonaltutorial godPersonaltutorial = godPersonaltutorialRepo
				.findOne(postPausePersonalAnswer.getPersonalTutorialID());
		if (godPersonaltutorial == null) {
			// No GodPersonaltutorial with specified id found.
			return ResponseEntity.status(HttpStatus.NOT_MODIFIED).body(new PostResult(false));
		}
		godPersonaltutorial.setUsedtime(postPausePersonalAnswer.getUsedtime());
		godPersonaltutorial.setLastseen(postPausePersonalAnswer.getLastseen());
		godPersonaltutorialRepo.save(godPersonaltutorial);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(new PostResult(true));
	}

	/*
	 * Returns the result of the specified PersonalTutorial
	 * (ToPersonalTutorialResult). If there is no GodPersonaltutorial for given the
	 * id the status code Not Modified (304) is returned.
	 */
	@GetMapping(value = "employee/personaltutorial/getResult/{id}", produces = "application/json")
	public ResponseEntity<ToPersonalTutorialResult> getResult(@PathVariable int id) {
		GodPersonaltutorial godPersonaltutorial = godPersonaltutorialRepo.findOne(id);
		if (godPersonaltutorial == null) {
			// No GodPersonaltutorial with specified id found.
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
		}
		int correctAnswers = 0;
		Set<GodPersonalanswer> godPersonalanswers = godPersonaltutorial.getGodSelectedanswers();
		Set<GodQuestion> godQuestions = godPersonaltutorial.getGodTutorial().getGodQuestions();
		for (GodQuestion godQuestion : godQuestions) {
			boolean falsecheck = true;
			Set<GodAnswer> godAnswers = godQuestion.getGodAnswers();
			for (GodAnswer godAnswer : godAnswers) {
				for (GodPersonalanswer godPersonalanswer : godPersonalanswers) {
					if (godPersonalanswer.getGodAnswer().getId() == godAnswer.getId()) {
						if (godPersonalanswer.isSelected() == godAnswer.getCorrect()) {
							falsecheck = false;
						} else {
							falsecheck = true;
							break;
						}
					}

				}
				if (falsecheck) {
					break;
				}
			}
			if (!falsecheck) {
				correctAnswers++;
			}
		}

		int totalAnswers = godPersonaltutorial.getGodTutorial().getGodQuestions().size();

		return ResponseEntity.status(HttpStatus.OK).body(new ToPersonalTutorialResult(correctAnswers, totalAnswers));
	}

	/*
	 * Returns a PDF Document encoded in Base64.
	 * If there is no GodPersonaltutorial for given the
	 * id or the GodPersonaltutorial is not done yet, the status code No Content (204) is returned.
	 */
	@GetMapping(value = "employee/personaltutorial/getPDF/{id}", produces = "application/json")
	public ResponseEntity<String> getPDF(@PathVariable int id) {
		GodPersonaltutorial tutorial = godPersonaltutorialRepo.findOne(id);
		if (tutorial == null) {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
		} else if (!tutorial.getState().equals("done")) {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
		}

		Comparator<GodQuestion> comp = new Comparator<GodQuestion>() {

			@Override
			public int compare(GodQuestion o1, GodQuestion o2) {
				if (o1.getOrder() < o2.getOrder())
					return -1;
				if (o1.getOrder() > o2.getOrder())
					return 1;
				return 0;
			}
		};

		LinkedList<GodQuestion> list = new LinkedList<>(tutorial.getGodTutorial().getGodQuestions());
		list.sort(comp);

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(tutorial.getDatefinished());
		String day = "" + calendar.get(Calendar.DAY_OF_MONTH);
		if (day.length() < 2) {
			day = '0' + day;
		}
		String month = "" + (calendar.get(Calendar.MONTH) + 1);
		if (month.length() < 2) {
			month = '0' + month;
		}
		String dateFinished = day + '.' + month + '.' + calendar.get(Calendar.YEAR);

		int correctAnswers = 0;
		Set<GodPersonalanswer> godPersonalanswers = tutorial.getGodSelectedanswers();
		Set<GodQuestion> godQuestions = tutorial.getGodTutorial().getGodQuestions();
		for (GodQuestion godQuestion : godQuestions) {
			boolean falsecheck = true;
			Set<GodAnswer> godAnswers = godQuestion.getGodAnswers();
			for (GodAnswer godAnswer : godAnswers) {
				for (GodPersonalanswer godPersonalanswer : godPersonalanswers) {
					if (godPersonalanswer.getGodAnswer().getId() == godAnswer.getId()) {
						if (godPersonalanswer.isSelected() == godAnswer.getCorrect()) {
							falsecheck = false;
						} else {
							falsecheck = true;
							break;
						}
					}

				}
				if (falsecheck) {
					break;
				}
			}
			if (!falsecheck) {
				correctAnswers++;
			}
		}
		int totalAnswers = tutorial.getGodTutorial().getGodQuestions().size();

		Context c = new Context();
		c.setVariable("tutorial", tutorial);
		c.setVariable("list", list);
		c.setVariable("dateFinished", dateFinished);
		c.setVariable("totalAnswers", totalAnswers);
		c.setVariable("correctAnswers", correctAnswers);
		c.setVariable("Encoder", Base64.getEncoder());
		c = Strings.includeStrings(c);
		String html = templateEngine.process("Results_pdf", c);

		ByteArrayOutputStream os = null;
		try {
			os = new ByteArrayOutputStream();
			ITextRenderer renderer = new ITextRenderer();
			renderer.getFontResolver().addFont("/fonts/arial.ttf", true);
			renderer.getSharedContext().setReplacedElementFactory(
					new MediaReplacedElementFactory((renderer.getSharedContext().getReplacedElementFactory())));
			renderer.setDocumentFromString(html);
			renderer.layout();
			renderer.createPDF(os, false);
			renderer.finishPDF();

			String retval = Base64.getEncoder().encodeToString(os.toByteArray());
			System.out.println("PDF created successfully:" + retval.length());
			return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_PDF)
					.header("Content-Disposition",
							"filename=" + tutorial.getGodTutorial().getTutorialname() + ' ' + dateFinished + ".pdf")
					.body(retval);
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (os != null) {
				try {
					os.close();

				} catch (IOException e) {
					/* ignore */ }
			}
		}
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);

	}

}
