package GET_Trained.restControllers;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Base64;

import javax.imageio.ImageIO;

import com.amazonaws.util.IOUtils;
import com.lowagie.text.Image;
import org.w3c.dom.Element;
import org.xhtmlrenderer.extend.FSImage;
import org.xhtmlrenderer.extend.ReplacedElement;
import org.xhtmlrenderer.extend.ReplacedElementFactory;
import org.xhtmlrenderer.extend.UserAgentCallback;
import org.xhtmlrenderer.layout.LayoutContext;
import org.xhtmlrenderer.pdf.ITextFSImage;
import org.xhtmlrenderer.pdf.ITextImageElement;
import org.xhtmlrenderer.render.BlockBox;
import org.xhtmlrenderer.simple.extend.FormSubmissionListener;


/**
 * Replaced element in order to replace elements like 
 * <tt>&lt;div class="media" data-src="image.png" /></tt> with the real
 * media content.
 */
public class MediaReplacedElementFactory implements ReplacedElementFactory {
    private final ReplacedElementFactory superFactory;

    public MediaReplacedElementFactory(ReplacedElementFactory superFactory) {
        this.superFactory = superFactory;
    }

    @Override
    public ReplacedElement createReplacedElement(LayoutContext layoutContext, BlockBox blockBox, UserAgentCallback userAgentCallback, int cssWidth, int cssHeight) {
        Element element = blockBox.getElement();
        if (element == null) {
            return null;
        }
        String nodeName = element.getNodeName();
        String className = element.getAttribute("class");
        // Replace any <div class="media" data-src="image.png" /> with the
        // binary data of `image.png` into the PDF.
        if ("div".equals(nodeName) && "media".equals(className)) {
            if (!element.hasAttribute("data-src")) {
                throw new RuntimeException("An element with class `media` is missing a `data-src` attribute indicating the media file.");
            }
            try {      	
            	java.net.URL img=this.getClass().getClassLoader().getResource(element.getAttribute("data-src"));
            	
                //input = new FileInputStream(element.getAttribute("data-src"));
                
                final Image image = Image.getInstance(img.getPath());
                final FSImage fsImage = new ITextFSImage(image);
                if (fsImage != null) {
                    if ((cssWidth != -1) || (cssHeight != -1)) {
                        fsImage.scale(cssWidth, cssHeight);
                    }
                    return new ITextImageElement(fsImage);
                }
            } catch (Exception e) {
                throw new RuntimeException("There was a problem trying to read a template embedded graphic.", e);
            } finally {
            	//IOUtils.closeQuietly(input, null);
            }
        }else {
        if("img".equals(nodeName) && "img_question".equals(className)) {
        	if(!element.hasAttribute("src")) {
        		 throw new RuntimeException("An element with class `img_question` is missing a `src` attribute indicating the media file.");
        	}
        	try {
        		String imageEncoded=element.getAttribute("src");
        		String imageBase64=imageEncoded.substring(imageEncoded.indexOf("base64")+"base64".length()+1);
        		
        		byte[]b=Base64.getDecoder().decode(imageBase64);
        		final Image image=Image.getInstance(b);
        		final FSImage fsImage=new ITextFSImage(image);
        		 if (fsImage != null) {
                     if ((cssWidth != -1) || (cssHeight != -1)) {
                         fsImage.scale(cssWidth, cssHeight);
                     }
                     return new ITextImageElement(fsImage);
                 }
        	} catch (Exception e) {
                throw new RuntimeException("There was a problem trying to read a template embedded graphic base64.", e);
            } finally {
            	//IOUtils.closeQuietly(input, null);
            }
        }}
        return this.superFactory.createReplacedElement(layoutContext, blockBox, userAgentCallback, cssWidth, cssHeight);
    }

    @Override
    public void reset() {
        this.superFactory.reset();
    }

    @Override
    public void remove(Element e) {
        this.superFactory.remove(e);
    }

    @Override
    public void setFormSubmissionListener(FormSubmissionListener listener) {
        this.superFactory.setFormSubmissionListener(listener);
    }
}