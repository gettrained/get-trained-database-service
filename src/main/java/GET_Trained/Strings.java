package GET_Trained;

import org.thymeleaf.context.Context;

public class Strings {
	public static final String pdf_employee="Mitarbeiter: ";
	public static final String pdf_finsihed_tutorial="Schulung abgeschlossen am: ";
	public static final String pdf_correct_questions_1=" von ";
	public static final String pdf_correct_questions_2=" Fragen korrekt";
	public static final String pdf_question="Frage ";
	
	public static Context includeStrings(Context c) {
		c.setVariable("pdf_employee", pdf_employee);
		c.setVariable("pdf_finsihed_tutorial", pdf_finsihed_tutorial);
		c.setVariable("pdf_correct_questions_1", pdf_correct_questions_1);
		c.setVariable("pdf_correct_questions_2", pdf_correct_questions_2);
		c.setVariable("pdf_question", pdf_question);
		
		return c;
	}
}
