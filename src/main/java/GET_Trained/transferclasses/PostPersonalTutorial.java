package GET_Trained.transferclasses;

import java.io.Serializable;

public class PostPersonalTutorial implements Serializable {
	private static final long serialVersionUID = 1L;

	private int employeeID;
	private int tutorialID;

	public PostPersonalTutorial() {
		super();
	}

	public PostPersonalTutorial(int employeeID, int tutorialID) {
		super();
		this.employeeID = employeeID;
		this.tutorialID = tutorialID;
	}

	public int getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(int employeeID) {
		this.employeeID = employeeID;
	}

	public int getTutorialID() {
		return tutorialID;
	}

	public void setTutorialID(int tutorialID) {
		this.tutorialID = tutorialID;
	}

}
