package GET_Trained.transferclasses;

import java.io.Serializable;

/**
 * Created by Pascal on 05.10.2017.
 */

public class ToTutorialMaterial implements Serializable{
    private static final long serialVersionUID = 1L;

    private int materialID;
    private int tutorialID;
    private String materialtype;
    private int order;

    public ToTutorialMaterial() {
    }
    public ToTutorialMaterial(int materialID, int tutorialID, String materialtype, int order) {
		super();
		this.materialID = materialID;
		this.tutorialID = tutorialID;
		this.materialtype = materialtype;
		this.order = order;
	}

	public String getMaterialtype() {
		return materialtype;
	}

	public void setMaterialtype(String materialtype) {
		this.materialtype = materialtype;
	}

	public int getMaterialID() {
        return materialID;
    }

    public void setMaterialID(int materialID) {
        this.materialID = materialID;
    }

    public int getTutorialID() {
        return tutorialID;
    }

    public void setTutorialID(int tutorialID) {
        this.tutorialID = tutorialID;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}
