package GET_Trained.transferclasses;

import java.io.Serializable;

public class ToLoginData implements Serializable {
	private static final long serialVersionUID = 1L;
	private String user;
	private	String hashedpw;

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getHashedpw() {
		return hashedpw;
	}

	public void setHashedpw(String hashedpw) {
		this.hashedpw = hashedpw;
	}

	@Override
	public String toString() {
		return "ToLoginData [user=" + user + ", hashedpw=" + hashedpw + "]";
	}

}
