package GET_Trained.transferclasses;

import java.io.Serializable;

public class PostResult implements Serializable {
	static final long serialVersionUID = 1L;

	private boolean result;

	public PostResult() {
		super();
	}

	public PostResult(boolean result) {
		super();
		this.result = result;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

}
