package GET_Trained.transferclasses;

import java.io.Serializable;

public class ToEmployee implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private String firstname;
	private String lastname;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	

	public ToEmployee() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ToEmployee(int id, String firstname, String lastname) {
		super();
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
	}

}
