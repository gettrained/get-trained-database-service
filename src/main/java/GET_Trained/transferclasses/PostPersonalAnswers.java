package GET_Trained.transferclasses;

import java.io.Serializable;
import java.util.LinkedList;

public class PostPersonalAnswers implements Serializable {
	private static final long serialVersionUID = 1L;

	private LinkedList<PostPersonalAnswer> personalanswers;

	public PostPersonalAnswers(LinkedList<PostPersonalAnswer> personalanswers) {
		super();
		this.personalanswers = personalanswers;
	}

	public PostPersonalAnswers() {
		super();
		this.personalanswers = new LinkedList<>();
	}

	public LinkedList<PostPersonalAnswer> getPersonalanswers() {
		return personalanswers;
	}

	public void setPersonalanswers(LinkedList<PostPersonalAnswer> personalanswers) {
		this.personalanswers = personalanswers;
	}
	
	
	
	
}
