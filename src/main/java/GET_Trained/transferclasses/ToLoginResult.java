package GET_Trained.transferclasses;

import java.io.Serializable;

public class ToLoginResult implements Serializable {
	private static final long serialVersionUID = 1L;
	private int employeeID;

	public int getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(int employeeID) {
		this.employeeID = employeeID;
	}

}
