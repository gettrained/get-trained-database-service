package GET_Trained.transferclasses;

import java.io.Serializable;

/**
 * Created by Pascal on 05.10.2017.
 */

public class ToTutorialVideo implements Serializable {
	private static final long serialVersionUID = 1L;

	private String video;

	public ToTutorialVideo() {
	}

	public ToTutorialVideo(String video) {
		super();
		this.video = video;
	}

	public String getVideo() {
		return video;
	}

	public void setVideo(String video) {
		this.video = video;
	}

}