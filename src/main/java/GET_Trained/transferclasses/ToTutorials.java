package GET_Trained.transferclasses;

import java.io.Serializable;
import java.util.LinkedList;

public class ToTutorials implements Serializable {
	private static final long serialVersionUID = 1L;

	private LinkedList<ToTutorial> tutorials = new LinkedList<>();

	public ToTutorials() {

	}

	public ToTutorials(LinkedList<ToTutorial> tutorials) {
		super();
		this.tutorials = tutorials;
	}

	public LinkedList<ToTutorial> getTutorials() {
		return tutorials;
	}

	public void setTutorials(LinkedList<ToTutorial> tutorials) {
		this.tutorials = tutorials;
	}

}
