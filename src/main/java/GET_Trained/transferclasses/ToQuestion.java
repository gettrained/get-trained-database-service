package GET_Trained.transferclasses;

import java.io.Serializable;
import java.util.LinkedList;

public class ToQuestion implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private byte[] byteStream;
	private String text;
	private int order;
	private LinkedList<ToAnswer>answers;
	
	public ToQuestion() {
		
	}
	
	
	
	public ToQuestion(int id, byte[] byteStream, String text, int order, LinkedList<ToAnswer> answers) {
		super();
		this.id = id;
		this.byteStream = byteStream;
		this.text = text;
		this.order = order;
		this.answers = answers;
	}

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public byte[] getByteStream() {
		return byteStream;
	}
	public void setByteStream(byte[] byteStream) {
		this.byteStream = byteStream;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}

	public LinkedList<ToAnswer> getAnswers() {
		return answers;
	}

	public void setAnswers(LinkedList<ToAnswer> answers) {
		this.answers = answers;
	}
}
