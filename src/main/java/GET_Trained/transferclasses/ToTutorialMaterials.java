package GET_Trained.transferclasses;

import java.io.Serializable;
import java.util.LinkedList;

public class ToTutorialMaterials implements Serializable {
	private static final long serialVersionUID = 1L;
	private LinkedList<ToTutorialMaterial> materials;

	public LinkedList<ToTutorialMaterial> getMaterials() {
		return materials;
	}

	public void setMaterials(LinkedList<ToTutorialMaterial> materials) {
		this.materials = materials;
	}

	public ToTutorialMaterials(LinkedList<ToTutorialMaterial> materials) {
		super();
		this.materials = materials;
	}

	public ToTutorialMaterials() {
		super();
	}

}
