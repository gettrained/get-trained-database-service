package GET_Trained.transferclasses;

import java.io.Serializable;
import java.util.LinkedList;

public class ToPersTutorials implements Serializable {
	private static final long serialVersionUID = 1L;
	private LinkedList<ToPersTutorial> perstutorials = new LinkedList<>();

	public LinkedList<ToPersTutorial> getPerstutorials() {
		return perstutorials;
	}

	public void setPerstutorials(LinkedList<ToPersTutorial> perstutorials) {
		this.perstutorials = perstutorials;
	}

	public ToPersTutorials() {
	}

	public ToPersTutorials(LinkedList<ToPersTutorial> perstutorials) {
		this.perstutorials = perstutorials;
	}

}
