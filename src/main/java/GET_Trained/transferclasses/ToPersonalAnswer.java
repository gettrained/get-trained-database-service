package GET_Trained.transferclasses;

public class ToPersonalAnswer {
	private int answerID;
	private boolean selected;
	
	
	
	public ToPersonalAnswer(int answerID, boolean selected) {
		super();
		this.answerID = answerID;
		this.selected = selected;
	}
	public int getAnswerID() {
		return answerID;
	}
	public void setAnswerID(int answerID) {
		this.answerID = answerID;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	

}
