package GET_Trained.domain;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The persistent class for the GMD_USERGROUPTUTORIAL database table.
 * 
 */
@Entity
@Table(name = "GMD_USERGROUPTUTORIAL")
@NamedQuery(name = "GmdUsergrouptutorial.findAll", query = "SELECT g FROM GmdUsergrouptutorial g")
public class GmdUsergrouptutorial implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;

	// bi-directional many-to-one association to GmdTutorial
	@ManyToOne
	@JoinColumn(name = "TUTORIAL_FK")
	private GmdTutorial gmdTutorial;

	// bi-directional many-to-one association to Usergroup
	@ManyToOne
	@JoinColumn(name = "USERGROUP_FK")
	@JsonIgnoreProperties("usergrouptutorials")
	private Usergroup usergroup;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public GmdTutorial getGmdTutorial() {
		return gmdTutorial;
	}

	public void setGmdTutorial(GmdTutorial gmdTutorial) {
		this.gmdTutorial = gmdTutorial;
	}

	public Usergroup getUsergroup() {
		return usergroup;
	}

	public void setUsergroup(Usergroup usergroup) {
		this.usergroup = usergroup;
	}

}
