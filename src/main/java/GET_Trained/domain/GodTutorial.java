package GET_Trained.domain;

import java.io.Serializable;
import javax.persistence.*;

import java.util.HashSet;
import java.util.Set;

/**
 * The persistent class for the GOD_TUTORIAL database table.
 * 
 */
@Entity
@Table(name = "GOD_TUTORIAL")
@NamedQuery(name = "GodTutorial.findAll", query = "SELECT g FROM GodTutorial g")
public class GodTutorial implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;

	@Column(name = "COLOR")
	private String color;

	@Column(name = "IDENTIFICATIONCODE")
	private int identificationcode;

	@Column(name = "SUCCESSPERCENTAGE")
	private int successpercentage;

	@Column(name = "TUTORIALNAME")
	private String tutorialname;

	@Column(name = "VALID")
	private boolean valid;

	@Column(name = "[VERSION]")
	private String version;

	// bi-directional many-to-one association to GodPersonaltutorial
	@OneToMany(mappedBy = "godTutorial")
	private Set<GodPersonaltutorial> godPersonaltutorials;

	// bi-directional many-to-one association to GodTutorialmaterial
	@OneToMany(mappedBy = "godTutorial")
	private Set<GodTutorialmaterial> godTutorialmaterials;

	// bi-directional many-to-one association to GodQuestion
	@OneToMany(mappedBy = "godTutorial")
	private Set<GodQuestion> godQuestions;
	
	public GodTutorial() {
		godTutorialmaterials = new HashSet<>();
		godPersonaltutorials = new HashSet<>();
	}

	
	
	public Set<GodQuestion> getGodQuestions() {
		return godQuestions;
	}



	public void setGodQuestions(Set<GodQuestion> godQuestions) {
		this.godQuestions = godQuestions;
	}



	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getColor() {
		return this.color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getIdentificationcode() {
		return this.identificationcode;
	}

	public void setIdentificationcode(int identificationcode) {
		this.identificationcode = identificationcode;
	}

	public int getSuccesspercentage() {
		return this.successpercentage;
	}

	public void setSuccesspercentage(int successpercentage) {
		this.successpercentage = successpercentage;
	}

	public String getTutorialname() {
		return this.tutorialname;
	}

	public void setTutorialname(String tutorialname) {
		this.tutorialname = tutorialname;
	}

	public boolean getValid() {
		return this.valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public String getVersion() {
		return this.version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Set<GodPersonaltutorial> getGodPersonaltutorials() {
		return this.godPersonaltutorials;
	}

	public void setGodPersonaltutorials(Set<GodPersonaltutorial> godPersonaltutorials) {
		this.godPersonaltutorials = godPersonaltutorials;
	}

	public GodPersonaltutorial addGodPersonaltutorial(GodPersonaltutorial godPersonaltutorial) {
		getGodPersonaltutorials().add(godPersonaltutorial);
		godPersonaltutorial.setGodTutorial(this);

		return godPersonaltutorial;
	}

	public GodPersonaltutorial removeGodPersonaltutorial(GodPersonaltutorial godPersonaltutorial) {
		getGodPersonaltutorials().remove(godPersonaltutorial);
		godPersonaltutorial.setGodTutorial(null);

		return godPersonaltutorial;
	}

	public Set<GodTutorialmaterial> getGodTutorialmaterials() {
		return this.godTutorialmaterials;
	}

	public void setGodTutorialmaterials(Set<GodTutorialmaterial> godTutorialmaterials) {
		this.godTutorialmaterials = godTutorialmaterials;
	}

	public GodTutorialmaterial addGodTutorialmaterial(GodTutorialmaterial godTutorialmaterial) {
		getGodTutorialmaterials().add(godTutorialmaterial);
		godTutorialmaterial.setGodTutorial(this);

		return godTutorialmaterial;
	}

	public GodTutorialmaterial removeGodTutorialmaterial(GodTutorialmaterial godTutorialmaterial) {
		getGodTutorialmaterials().remove(godTutorialmaterial);
		godTutorialmaterial.setGodTutorial(null);

		return godTutorialmaterial;
	}

	public GodTutorial(String color, int identificationcode, int successpercentage, String tutorialname,
			boolean valid, String version, Set<GodPersonaltutorial> godPersonaltutorials,
			Set<GodTutorialmaterial> godTutorialmaterials) {
		super();
		this.color = color;
		this.identificationcode = identificationcode;
		this.successpercentage = successpercentage;
		this.tutorialname = tutorialname;
		this.valid = valid;
		this.version = version;
		this.godPersonaltutorials = godPersonaltutorials;
		this.godTutorialmaterials = godTutorialmaterials;
	}

	
	
}