package GET_Trained.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the EMPLOYEE database table .
 * 
 */
@Entity
@Table(name = "EMPLOYEE")
@NamedQuery(name = "Employee.findAll", query = "SELECT e FROM Employee e")
public class Employee implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;

	@Column(name = "ALLOWADLOGIN")
	private short allowadlogin;

	@Column(name = "ALLOWGSMSLOGIN")
	private short allowgsmslogin;

	@Column(name = "DEFAULTLOCATIONID")
	private int defaultlocationid;

	@Column(name = "EMPLOYEENUMBER")
	private String employeenumber;

	@Column(name = "FIRSTNAME")
	private String firstname;

	@Column(name = "GSMSDIRECTORYOBJECTID")
	private int gsmsdirectoryobjectid;

	@Lob
	@Column(name = "IMAGE")
	private byte[] image;

	@Column(name = "ISADUSER")
	private short isaduser;

	@Column(name = "ISENABLED")
	private short isenabled;

	@Column(name = "ISFIRSTLOGIN")
	private short isfirstlogin;

	@Column(name = "ISSYSTEMUSER")
	private short issystemuser;

	@Column(name = "LASTLOGIN")
	private Timestamp lastlogin;

	@Column(name = "LASTNAME")
	private String lastname;

	@Column(name = "LOGIN")
	private String login;

	@Column(name = "PASSWORDHASH")
	private String passwordhash;

	@Column(name = "PASSWORDSEED")
	private String passwordseed;

	@Column(name = "PASSWORDTYPE")
	private int passwordtype;

	@Column(name = "PREFERREDCULTUREID")
	private int preferredcultureid;

	@Column(name = "PROFESSION")
	private String profession;

	@Column(name = "TITLE")
	private String title;

	@Column(name = "UDELETE")
	private short udelete;

	@Column(name = "USESSION")
	private int usession;

	@Column(name = "UTIME")
	private Timestamp utime;

	@Column(name = "UUSER")
	private String uuser;

	@Column(name = "UVERSION")
	private long uversion;

	@ManyToMany
	@JoinTable(name = "USERGROUPMEMBER", joinColumns = @JoinColumn(name = "EMPLOYEE_FK", referencedColumnName = "ID"), inverseJoinColumns = @JoinColumn(name = "USERGROUP_FK", referencedColumnName = "ID"))
	private Set<Usergroup> usergroups;

	// // bi-directional many-to-one association to Usergroupmember
	// @OneToMany(mappedBy = "employee")
	// private Set<Usergroupmember> usergroupmembers;

	// bi-directional many-to-one association to GodPersonaltutorial
	@OneToMany(mappedBy = "employee")
	private Set<GodPersonaltutorial> godPersonaltutorials;

	public Employee() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public short getAllowadlogin() {
		return this.allowadlogin;
	}

	public void setAllowadlogin(short allowadlogin) {
		this.allowadlogin = allowadlogin;
	}

	public short getAllowgsmslogin() {
		return this.allowgsmslogin;
	}

	public void setAllowgsmslogin(short allowgsmslogin) {
		this.allowgsmslogin = allowgsmslogin;
	}

	public int getDefaultlocationid() {
		return this.defaultlocationid;
	}

	public void setDefaultlocationid(int defaultlocationid) {
		this.defaultlocationid = defaultlocationid;
	}

	public String getEmployeenumber() {
		return this.employeenumber;
	}

	public void setEmployeenumber(String employeenumber) {
		this.employeenumber = employeenumber;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public int getGsmsdirectoryobjectid() {
		return this.gsmsdirectoryobjectid;
	}

	public void setGsmsdirectoryobjectid(int gsmsdirectoryobjectid) {
		this.gsmsdirectoryobjectid = gsmsdirectoryobjectid;
	}

	public byte[] getImage() {
		return this.image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public short getIsaduser() {
		return this.isaduser;
	}

	public void setIsaduser(short isaduser) {
		this.isaduser = isaduser;
	}

	public short getIsenabled() {
		return this.isenabled;
	}

	public void setIsenabled(short isenabled) {
		this.isenabled = isenabled;
	}

	public short getIsfirstlogin() {
		return this.isfirstlogin;
	}

	public void setIsfirstlogin(short isfirstlogin) {
		this.isfirstlogin = isfirstlogin;
	}

	public short getIssystemuser() {
		return this.issystemuser;
	}

	public void setIssystemuser(short issystemuser) {
		this.issystemuser = issystemuser;
	}

	public Timestamp getLastlogin() {
		return this.lastlogin;
	}

	public void setLastlogin(Timestamp lastlogin) {
		this.lastlogin = lastlogin;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPasswordhash() {
		return this.passwordhash;
	}

	public void setPasswordhash(String passwordhash) {
		this.passwordhash = passwordhash;
	}

	public String getPasswordseed() {
		return this.passwordseed;
	}

	public void setPasswordseed(String passwordseed) {
		this.passwordseed = passwordseed;
	}

	public int getPasswordtype() {
		return this.passwordtype;
	}

	public void setPasswordtype(int passwordtype) {
		this.passwordtype = passwordtype;
	}

	public int getPreferredcultureid() {
		return this.preferredcultureid;
	}

	public void setPreferredcultureid(int preferredcultureid) {
		this.preferredcultureid = preferredcultureid;
	}

	public String getProfession() {
		return this.profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public short getUdelete() {
		return this.udelete;
	}

	public void setUdelete(short udelete) {
		this.udelete = udelete;
	}

	public int getUsession() {
		return this.usession;
	}

	public void setUsession(int usession) {
		this.usession = usession;
	}

	public Timestamp getUtime() {
		return this.utime;
	}

	public void setUtime(Timestamp utime) {
		this.utime = utime;
	}

	public String getUuser() {
		return this.uuser;
	}

	public void setUuser(String uuser) {
		this.uuser = uuser;
	}

	public long getUversion() {
		return this.uversion;
	}

	public void setUversion(long uversion) {
		this.uversion = uversion;
	}

	public Set<Usergroup> getUsergroups() {
		return usergroups;
	}

	public void setUsergroups(Set<Usergroup> usergroups) {
		this.usergroups = usergroups;
	}
	//
	// public Usergroup addUsergroup(Usergroup usergroup) {
	// getUsergroups().add(usergroup);
	// usergroup.setEmployee(this);
	//
	// return usergroup;
	// }
	//
	// public Set<Usergroupmember> getUsergroupmembers() {
	// return this.usergroupmembers;
	// }
	//

	//
	// public void setUsergroupmembers(Set<Usergroupmember> usergroupmembers) {
	// this.usergroupmembers = usergroupmembers;
	// }
	//
	// public Set<Usergroupmember> getUsergroupmembers() {
	// return usergroupmembers;
	// }
	//
	// public Usergroupmember addUsergroupmember(Usergroupmember usergroupmember) {
	// getUsergroupmembers().add(usergroupmember);
	// usergroupmember.setEmployee(this);
	//
	// return usergroupmember;
	// }
	//
	// public Usergroupmember removeUsergroupmember(Usergroupmember usergroupmember)
	// {
	// getUsergroupmembers().remove(usergroupmember);
	// usergroupmember.setEmployee(null);
	//
	// return usergroupmember;
	// }

	public Set<GodPersonaltutorial> getGodPersonaltutorials() {
		return this.godPersonaltutorials;
	}

	public void setGodPersonaltutorials(Set<GodPersonaltutorial> godPersonaltutorials) {
		this.godPersonaltutorials = godPersonaltutorials;
	}

	public GodPersonaltutorial addGodPersonaltutorial(GodPersonaltutorial godPersonaltutorial) {
		getGodPersonaltutorials().add(godPersonaltutorial);
		godPersonaltutorial.setEmployee(this);

		return godPersonaltutorial;
	}

	public GodPersonaltutorial removeGodPersonaltutorial(GodPersonaltutorial godPersonaltutorial) {
		getGodPersonaltutorials().remove(godPersonaltutorial);
		godPersonaltutorial.setEmployee(null);

		return godPersonaltutorial;
	}

}