package GET_Trained.domain;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the GOD_ANSWER database table.
 * 
 */
@Entity
@Table(name = "GOD_ANSWER")
@NamedQuery(name = "GodAnswer.findAll", query = "SELECT g FROM GodAnswer g")
public class GodAnswer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;

	@Column(name = "CORRECT")
	private boolean correct;

	@Column(name = "ANSWERTEXT")
	private String answertext;

	// bi-directional many-to-one association to GodQuestion
	@ManyToOne
	@JoinColumn(name = "QUESTION_FK")
	private GodQuestion godQuestion;

	@OneToMany(mappedBy = "godAnswer")
	private Set<GodPersonalanswer> godPersonalanswers;
	
	public GodAnswer() {
		super();
	}

	public GodAnswer(boolean correct, String answertext, GodQuestion godQuestion) {
		super();
		this.correct = correct;
		this.answertext = answertext;
		this.godQuestion = godQuestion;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean getCorrect() {
		return correct;
	}

	public void setCorrect(boolean correct) {
		this.correct = correct;
	}

	public String getAnswertext() {
		return answertext;
	}

	public void setAnswertext(String answertext) {
		this.answertext = answertext;
	}

	public GodQuestion getGodQuestion() {
		return godQuestion;
	}

	public void setGodQuestion(GodQuestion godQuestion) {
		this.godQuestion = godQuestion;
	}

	public Set<GodPersonalanswer> getGodPersonalanswers() {
		return godPersonalanswers;
	}

	public GodPersonalanswer getGodPersonalanswertoPersonalTutoral(int id){
		// Id is GodPersonalTutorialID
		
		LinkedList<GodPersonalanswer> godPersonalanswers = new LinkedList<GodPersonalanswer>();
		
		for(GodPersonalanswer item: this.godPersonalanswers ) {
			if (item.getGodPersonaltutorial().getId() == id) {
				return item;	
			}
		}
		 return null;
	}
	
	public void setGodPersonalanswers(Set<GodPersonalanswer> godPersonalanswers) {
		this.godPersonalanswers = godPersonalanswers;
	}
	
	

}
