package GET_Trained.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;

/**
 * The persistent class for the GMD_TUTORIALTYPE database table.
 * 
 */
@Entity
@Table(name = "GMD_TUTORIALTYPE")
@NamedQuery(name = "GmdTutorialtype.findAll", query = "SELECT g FROM GmdTutorialtype g")
public class GmdTutorialtype implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;

	@Column(name = "[TYPE]")
	private String type;

	// bi-directional many-to-one association to GmdTutorialmaterial
	@OneToMany(mappedBy = "gmdTutorialtype")
	private Set<GmdTutorialmaterial> gmdTutorialmaterials;

	public GmdTutorialtype() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Set<GmdTutorialmaterial> getGmdTutorialmaterials() {
		return this.gmdTutorialmaterials;
	}

	public void setGmdTutorialmaterials(Set<GmdTutorialmaterial> gmdTutorialmaterials) {
		this.gmdTutorialmaterials = gmdTutorialmaterials;
	}

	public GmdTutorialmaterial addGmdTutorialmaterial(GmdTutorialmaterial gmdTutorialmaterial) {
		getGmdTutorialmaterials().add(gmdTutorialmaterial);
		gmdTutorialmaterial.setGmdTutorialtype(this);

		return gmdTutorialmaterial;
	}

	public GmdTutorialmaterial removeGmdTutorialmaterial(GmdTutorialmaterial gmdTutorialmaterial) {
		getGmdTutorialmaterials().remove(gmdTutorialmaterial);
		gmdTutorialmaterial.setGmdTutorialtype(null);

		return gmdTutorialmaterial;
	}

}