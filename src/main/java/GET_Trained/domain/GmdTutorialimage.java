package GET_Trained.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the GMD_TUTORIALIMAGE database table.
 * 
 */
@Entity
@Table(name = "GMD_TUTORIALIMAGE")
@NamedQuery(name = "GmdTutorialimage.findAll", query = "SELECT g FROM GmdTutorialimage g")
public class GmdTutorialimage implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;

	@Lob
	@Column(name = "IMAGE")
	private byte[] image;

	@Column(name = "TEXT")
	private String text;

	// bi-directional one-to-one association to GmdTutorialmaterial
	@OneToOne(mappedBy = "gmdTutorialimage")
	private GmdTutorialmaterial gmdTutorialmaterial;

	public GmdTutorialimage() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte[] getImage() {
		return this.image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public GmdTutorialmaterial getGmdTutorialmaterial() {
		return this.gmdTutorialmaterial;
	}

	public void setGmdTutorialmaterial(GmdTutorialmaterial gmdTutorialmaterial) {
		this.gmdTutorialmaterial = gmdTutorialmaterial;
	}

}