package GET_Trained.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the GMD_TUTORIAL database table.
 * 
 */
@Entity
@Table(name = "GMD_TUTORIAL")
@NamedQuery(name = "GmdTutorial.findAll", query = "SELECT g FROM GmdTutorial g")
public class GmdTutorial implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;

	@Column(name = "COLOR")
	private String color;

	@Column(name = "IDENTIFICATIONCODE")
	private int identificationcode;

	@Column(name = "SUCCESSPERCENTAGE")
	private int successpercentage;

	@Column(name = "TUTORIALNAME")
	private String tutorialname;

	@Column(name = "VALID")
	private boolean valid;

	@Column(name = "[VERSION]")
	private String version;

	// bi-directional many-to-one association to GmdQuestion
	@OneToMany(mappedBy = "gmdTutorial")
	private Set<GmdQuestion> gmdQuestions;

	// bi-directional many-to-one association to GmdUsergrouptutorial
//	@OneToMany(mappedBy = "gmdTutorial")
//	private Set<GmdUsergrouptutorial> gmdUsergrouptutorials;
	
	
	@ManyToMany(mappedBy="gmdtutorials")
	private Set<Usergroup> usergroups;
	

	// bi-directional many-to-one association to GmdTutorialmaterial
	@OneToMany(mappedBy = "gmdTutorial")
	private Set<GmdTutorialmaterial> gmdTutorialmaterials;

	public GmdTutorial() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getColor() {
		return this.color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getIdentificationcode() {
		return this.identificationcode;
	}

	public void setIdentificationcode(int identificationcode) {
		this.identificationcode = identificationcode;
	}

	public int getSuccesspercentage() {
		return this.successpercentage;
	}

	public void setSuccesspercentage(int successpercentage) {
		this.successpercentage = successpercentage;
	}

	public String getTutorialname() {
		return this.tutorialname;
	}

	public void setTutorialname(String tutorialname) {
		this.tutorialname = tutorialname;
	}

	public boolean getValid() {
		return this.valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public String getVersion() {
		return this.version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Set<GmdQuestion> getGmdQuestions() {
		return this.gmdQuestions;
	}

	public void setGmdQuestions(Set<GmdQuestion> gmdQuestions) {
		this.gmdQuestions = gmdQuestions;
	}

	public GmdQuestion addGmdQuestions(GmdQuestion gmdQuestions) {
		getGmdQuestions().add(gmdQuestions);
		gmdQuestions.setGmdTutorial(this);

		return gmdQuestions;
	}

	public GmdQuestion removeGmdQuestions(GmdQuestion gmdQuestions) {
		getGmdQuestions().remove(gmdQuestions);
		gmdQuestions.setGmdTutorial(null);

		return gmdQuestions;
	}

//	public Set<GmdUsergrouptutorial> getGmdUsergrouptutorials() {
//		return this.gmdUsergrouptutorials;
//	}
//
//	public void setGmdUsergrouptutorials(Set<GmdUsergrouptutorial> gmdUsergrouptutorials) {
//		this.gmdUsergrouptutorials = gmdUsergrouptutorials;
//	}
//
//	public GmdUsergrouptutorial addGmdUsergrouptutorial(GmdUsergrouptutorial gmdUsergrouptutorial) {
//		getGmdUsergrouptutorials().add(gmdUsergrouptutorial);
//		gmdUsergrouptutorial.setGmdTutorial(this);
//
//		return gmdUsergrouptutorial;
//	}
//
//	public GmdUsergrouptutorial removeGmdUsergrouptutorial(GmdUsergrouptutorial gmdUsergrouptutorial) {
//		getGmdUsergrouptutorials().remove(gmdUsergrouptutorial);
//		gmdUsergrouptutorial.setGmdTutorial(null);
//
//		return gmdUsergrouptutorial;
//	}
	
	

	public Set<GmdTutorialmaterial> getGmdTutorialmaterials() {
		return this.gmdTutorialmaterials;
	}

	public Set<Usergroup> getUsergroups() {
		return usergroups;
	}

	public void setUsergroups(Set<Usergroup> usergroups) {
		this.usergroups = usergroups;
	}

	public void setGmdTutorialmaterials(Set<GmdTutorialmaterial> gmdTutorialmaterials) {
		this.gmdTutorialmaterials = gmdTutorialmaterials;
	}

	public GmdTutorialmaterial addGmdTutorialmaterial(GmdTutorialmaterial gmdTutorialmaterial) {
		getGmdTutorialmaterials().add(gmdTutorialmaterial);
		gmdTutorialmaterial.setGmdTutorial(this);

		return gmdTutorialmaterial;
	}

	public GmdTutorialmaterial removeGmdTutorialmaterial(GmdTutorialmaterial gmdTutorialmaterial) {
		getGmdTutorialmaterials().remove(gmdTutorialmaterial);
		gmdTutorialmaterial.setGmdTutorial(null);

		return gmdTutorialmaterial;
	}

}