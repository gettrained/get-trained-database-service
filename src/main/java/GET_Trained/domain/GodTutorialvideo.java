package GET_Trained.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the GOD_TUTORIALVIDEO database table.
 * 
 */
@Entity
@Table(name = "GOD_TUTORIALVIDEO")
@NamedQuery(name = "GodTutorialvideo.findAll", query = "SELECT g FROM GodTutorialvideo g")
public class GodTutorialvideo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;

	@Column(name = "VIDEO")
	private String video;

	// bi-directional one-to-one association to GodTutorialmaterial
	@OneToOne(mappedBy = "godTutorialvideo")
	private GodTutorialmaterial godTutorialmaterial;

	public GodTutorialvideo() {
	}

	
	
	public GodTutorialvideo(String video) {
		super();
		this.video = video;
	}



	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getVideo() {
		return video;
	}



	public void setVideo(String video) {
		this.video = video;
	}



	public GodTutorialmaterial getGodTutorialmaterial() {
		return this.godTutorialmaterial;
	}

	public void setGodTutorialmaterial(GodTutorialmaterial godTutorialmaterial) {
		this.godTutorialmaterial = godTutorialmaterial;
	}

}