package GET_Trained.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;

/**
 * The persistent class for the GOD_TUTORIALTYPE database table.
 * 
 */
@Entity
@Table(name = "GOD_TUTORIALTYPE")
@NamedQuery(name = "GodTutorialtype.findAll", query = "SELECT g FROM GodTutorialtype g")
public class GodTutorialtype implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;

	@Column(name = "[TYPE]")
	private String type;

	// bi-directional Many-to-one association to GodTutorialmaterial
	@OneToMany(mappedBy = "godTutorialtype")
	private Set<GodTutorialmaterial> godTutorialmaterials;

	public GodTutorialtype() {
	}

	
	
	public GodTutorialtype(String type) {
		super();
		this.type = type;
	}



	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Set<GodTutorialmaterial> getgodTutorialmaterials() {
		return this.godTutorialmaterials;
	}

	public void setGodAnswers(Set<GodTutorialmaterial> godTutorialmaterials) {
		this.godTutorialmaterials = godTutorialmaterials;
	}

	public GodTutorialmaterial addGodAnswer(GodTutorialmaterial godTutorialmaterial) {
		getgodTutorialmaterials().add(godTutorialmaterial);
		godTutorialmaterial.setGodTutorialtype(this);

		return godTutorialmaterial;
	}

	public GodTutorialmaterial removeGodAnswer(GodTutorialmaterial godTutorialmaterial) {
		getgodTutorialmaterials().remove(godTutorialmaterial);
		godTutorialmaterial.setGodTutorialtype(null);

		return godTutorialmaterial;
	}

}