package GET_Trained.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the GOD_TUTORIALIMAGE database table.
 * 
 */
@Entity
@Table(name = "GOD_TUTORIALIMAGE")
@NamedQuery(name = "GodTutorialimage.findAll", query = "SELECT g FROM GodTutorialimage g")
public class GodTutorialimage implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;

	@Lob
	@Column(name = "IMAGE")
	private byte[] image;

	@Column(name = "TEXT")
	private String text;

	// bi-directional one-to-one association to GodTutorialmaterial
	@OneToOne(mappedBy = "godTutorialimage")
	private GodTutorialmaterial godTutorialmaterial;
	
	

	public GodTutorialimage(byte[] image, String text) {
		super();
		this.image = image;
		this.text = text;
	}

	public GodTutorialimage() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte[] getImage() {
		return this.image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public GodTutorialmaterial getGodTutorialmaterial() {
		return this.godTutorialmaterial;
	}

	public void setGodTutorialmaterial(GodTutorialmaterial godTutorialmaterial) {
		this.godTutorialmaterial = godTutorialmaterial;
	}

}