package GET_Trained.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the GMD_TUTORIALMATERIAL database table.
 * 
 */
@Entity
@Table(name = "GOD_TUTORIALMATERIAL")
@NamedQuery(name = "GodTutorialmaterial.findAll", query = "SELECT g FROM GodTutorialmaterial g")
public class GodTutorialmaterial implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;

	// bi-directional many-to-one association to GodTutorial
	@ManyToOne
	@JoinColumn(name = "TUTORIAL_FK")
	private GodTutorial godTutorial;

	@Column(name = "[ORDER]")
	private int order;

	// bi-directional many-to-one association to GodTutorialtype
	@ManyToOne
	@JoinColumn(name = "TYPE_FK")
	private GodTutorialtype godTutorialtype;

	// bi-directional many-to-one association to GodTUtorialvideo
	@OneToOne
	@JoinColumn(name = "VIDEO_FK")
	private GodTutorialvideo godTutorialvideo;

	// bi-directional many-to-one association to GodTutorialimgae
	@OneToOne
	@JoinColumn(name = "IMAGE_FK")
	private GodTutorialimage godTutorialimage;

	
	
	public GodTutorialmaterial() {
		super();
	}



	public GodTutorialmaterial(GodTutorial godTutorial, int order, GodTutorialtype godTutorialtype,
			GodTutorialimage godTutorialimage) {
		super();
		this.godTutorial = godTutorial;
		this.order = order;
		this.godTutorialtype = godTutorialtype;
		this.godTutorialimage = godTutorialimage;
	}
	
	

	public GodTutorialmaterial(GodTutorial godTutorial, int order, GodTutorialtype godTutorialtype,
			GodTutorialvideo godTutorialvideo) {
		super();
		this.godTutorial = godTutorial;
		this.order = order;
		this.godTutorialtype = godTutorialtype;
		this.godTutorialvideo = godTutorialvideo;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public GodTutorial getGodTutorial() {
		return godTutorial;
	}

	public void setGodTutorial(GodTutorial godTutorial) {
		this.godTutorial = godTutorial;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public GodTutorialtype getGodTutorialtype() {
		return godTutorialtype;
	}

	public void setGodTutorialtype(GodTutorialtype godTutorialtype) {
		this.godTutorialtype = godTutorialtype;
	}

	public GodTutorialvideo getGodTutorialvideo() {
		return godTutorialvideo;
	}

	public void setGodTutorialvideo(GodTutorialvideo godTutorialvideo) {
		this.godTutorialvideo = godTutorialvideo;
	}

	public GodTutorialimage getGodTutorialimage() {
		return godTutorialimage;
	}

	public void setGodTutorialimage(GodTutorialimage godTutorialimage) {
		this.godTutorialimage = godTutorialimage;
	}

}
