package GET_Trained.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the GMD_ANSWER database table.
 * 
 */
@Entity
@Table(name = "GMD_ANSWER")
@NamedQuery(name = "GmdAnswer.findAll", query = "SELECT g FROM GmdAnswer g")
public class GmdAnswer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;

	@Column(name = "CORRECT")
	private boolean correct;

	@Column(name = "ANSWERTEXT")
	private String answertext;

	// bi-directional many-to-one association to GmdQuestion
	@ManyToOne
	@JoinColumn(name = "QUESTION_FK")
	private GmdQuestion gmdQuestion;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean getCorrect() {
		return correct;
	}

	public void setCorrect(boolean correct) {
		this.correct = correct;
	}

	public String getAnswertext() {
		return answertext;
	}

	public void setAnswertext(String answertext) {
		this.answertext = answertext;
	}

	public GmdQuestion getGmdQuestion() {
		return gmdQuestion;
	}

	public void setGmdQuestion(GmdQuestion gmdQuestion) {
		this.gmdQuestion = gmdQuestion;
	}

}
