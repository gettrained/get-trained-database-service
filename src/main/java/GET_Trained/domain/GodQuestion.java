package GET_Trained.domain;

import java.io.Serializable;
import javax.persistence.*;

import java.util.HashSet;
import java.util.Set;

/**
 * The persistent class for the GMD_QUESTION database table.
 * 
 */
@Entity
@Table(name = "GOD_QUESTION")
@NamedQuery(name = "GodQuestion.findAll", query = "SELECT g FROM GodQuestion g")
public class GodQuestion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;

	@Lob
	@Column(name = "IMAGE")
	private byte[] image;

	@Column(name = "TEXT")
	private String text;

	@Column(name = "[ORDER]")
	private int order;

	// bi-directional many-to-one association to GodTutorial
	@ManyToOne
	@JoinColumn(name = "TUTORIAL_FK")
	private GodTutorial godTutorial;

	// bi-directional many-to-one association to GodAnswer
	@OneToMany(mappedBy = "godQuestion")
	private Set<GodAnswer> godAnswers;

	public GodQuestion() {
		godAnswers = new HashSet<>();
	}

	public GodQuestion(byte[] image, String text, int order, GodTutorial godTutorial,
			Set<GodAnswer> godAnswers) {
		super();
		this.image = image;
		this.text = text;
		this.order = order;
		this.godTutorial = godTutorial;
		this.godAnswers = godAnswers;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte[] getImage() {
		return this.image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public GodTutorial getGodTutorial() {
		return this.godTutorial;
	}

	public void setGodTutorial(GodTutorial godTutorial) {
		this.godTutorial = godTutorial;
	}

	public Set<GodAnswer> getGodAnswers() {
		return this.godAnswers;
	}

	public void setGodAnswers(Set<GodAnswer> godAnswers) {
		this.godAnswers = godAnswers;
	}

	public GodAnswer addGodAnswer(GodAnswer godAnswer) {
		getGodAnswers().add(godAnswer);
		godAnswer.setGodQuestion(this);

		return godAnswer;
	}

	public GodAnswer removeGodAnswer(GodAnswer godAnswer) {
		getGodAnswers().remove(godAnswer);
		godAnswer.setGodQuestion(null);

		return godAnswer;
	}

}
