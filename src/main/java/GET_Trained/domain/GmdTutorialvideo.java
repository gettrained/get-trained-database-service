package GET_Trained.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the GMD_TUTORIALVIDEO database table.
 * 
 */
@Entity
@Table(name = "GMD_TUTORIALVIDEO")
@NamedQuery(name = "GmdTutorialvideo.findAll", query = "SELECT g FROM GmdTutorialvideo g")
public class GmdTutorialvideo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;

	@Column(name = "VIDEO")
	private String video;

	// bi-directional one-to-one association to GmdTutorialmaterial
	@OneToOne(mappedBy = "gmdTutorialvideo")
	private GmdTutorialmaterial gmdTutorialmaterial;

	public GmdTutorialvideo() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getVideo() {
		return video;
	}

	public void setVideo(String video) {
		this.video = video;
	}

	public GmdTutorialmaterial getGmdTutorialmaterial() {
		return this.gmdTutorialmaterial;
	}

	public void setGmdTutorialmaterial(GmdTutorialmaterial gmdTutorialmaterial) {
		this.gmdTutorialmaterial = gmdTutorialmaterial;
	}

}