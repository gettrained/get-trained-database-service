package GET_Trained.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;

/**
 * The persistent class for the GMD_QUESTION database table.
 * 
 */
@Entity
@Table(name = "GMD_QUESTION")
@NamedQuery(name = "GmdQuestion.findAll", query = "SELECT g FROM GmdQuestion g")
public class GmdQuestion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;

	@Lob
	@Column(name = "IMAGE")
	private byte[] image;

	@Column(name = "TEXT")
	private String text;
	
	@Column(name = "[ORDER]")
	private int order;

	// bi-directional many-to-one association to GmdTutorial
	@ManyToOne
	@JoinColumn(name = "TUTORIAL_FK")
	private GmdTutorial gmdTutorial;

	// bi-directional many-to-one association to GmdAnswer
	@OneToMany(mappedBy = "gmdQuestion")
	private Set<GmdAnswer> gmdAnswers;

	public GmdQuestion() {
	}

	

	public int getOrder() {
		return order;
	}



	public void setOrder(int order) {
		this.order = order;
	}



	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte[] getImage() {
		return this.image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public GmdTutorial getGmdTutorial() {
		return this.gmdTutorial;
	}

	public void setGmdTutorial(GmdTutorial gmdTutorial) {
		this.gmdTutorial = gmdTutorial;
	}

	public Set<GmdAnswer> getGmdAnswers() {
		return this.gmdAnswers;
	}

	public void setGmdAnswers(Set<GmdAnswer> gmdAnswers) {
		this.gmdAnswers = gmdAnswers;
	}

	public GmdAnswer addGmdAnswer(GmdAnswer gmdAnswer) {
		getGmdAnswers().add(gmdAnswer);
		gmdAnswer.setGmdQuestion(this);

		return gmdAnswer;
	}

	public GmdAnswer removeGmdAnswer(GmdAnswer gmdAnswer) {
		getGmdAnswers().remove(gmdAnswer);
		gmdAnswer.setGmdQuestion(null);

		return gmdAnswer;
	}

}