package GET_Trained.domain;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the USERGROUP database table.
 * 
 */
@Entity
@Table(name="USERGROUP")
@NamedQuery(name="Usergroup.findAll", query="SELECT u FROM Usergroup u")
public class Usergroup  {
	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;

	@Column(name="ADUSERGROUPNAME")
	private String adusergroupname;

	@Column(name="DESCRIPTION")
	private String description;

	@Column(name="GSMSDIRECTORYOBJECTID")
	private int gsmsdirectoryobjectid;

	@Column(name="ISADUSERGROUPMAPPED")
	private short isadusergroupmapped;

	@Column(name="ISDEFAULTGROUP")
	private short isdefaultgroup;

	@Column(name="ISSYTEMGROUP")
	private short issytemgroup;

	@Column(name="NAME")
	private String name;

	@Column(name="UDELETE")
	private short udelete;

	@Column(name="USESSION")
	private int usession;

	@Column(name="UTIME")
	private Timestamp utime;

	@Column(name="UUSER")
	private String uuser;

	@Column(name="UVERSION")
	private long uversion;

//	//bi-directional many-to-one association to Usergroupmember
//	@OneToMany(mappedBy="usergroup")
//	private Set<Usergroupmember> usergroupmembers;
	
	@ManyToMany(mappedBy="usergroups")
	private Set<Employee> employees;
	
	//bi-directional many-to-one association to GmdUsergrouptutorial
//	@OneToMany(mappedBy = "usergroup")
//	@JsonIgnoreProperties("gmdUsergrouptutorials")
//	private Set<GmdUsergrouptutorial> gmdUsergrouptutorials;
	
	@ManyToMany
	  @JoinTable(
	      name="GMD_USERGROUPTUTORIAL",
	      joinColumns=@JoinColumn(name="USERGROUP_FK", referencedColumnName="ID"),
	      inverseJoinColumns=@JoinColumn(name="TUTORIAL_FK", referencedColumnName="ID"))
	private Set<GmdTutorial> gmdtutorials;

	public Usergroup() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAdusergroupname() {
		return this.adusergroupname;
	}

	public void setAdusergroupname(String adusergroupname) {
		this.adusergroupname = adusergroupname;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getGsmsdirectoryobjectid() {
		return this.gsmsdirectoryobjectid;
	}

	public void setGsmsdirectoryobjectid(int gsmsdirectoryobjectid) {
		this.gsmsdirectoryobjectid = gsmsdirectoryobjectid;
	}

	public short getIsadusergroupmapped() {
		return this.isadusergroupmapped;
	}

	public void setIsadusergroupmapped(short isadusergroupmapped) {
		this.isadusergroupmapped = isadusergroupmapped;
	}

	public short getIsdefaultgroup() {
		return this.isdefaultgroup;
	}

	public void setIsdefaultgroup(short isdefaultgroup) {
		this.isdefaultgroup = isdefaultgroup;
	}

	public short getIssytemgroup() {
		return this.issytemgroup;
	}

	public void setIssytemgroup(short issytemgroup) {
		this.issytemgroup = issytemgroup;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public short getUdelete() {
		return this.udelete;
	}

	public void setUdelete(short udelete) {
		this.udelete = udelete;
	}

	public int getUsession() {
		return this.usession;
	}

	public void setUsession(int usession) {
		this.usession = usession;
	}

	public Timestamp getUtime() {
		return this.utime;
	}

	public void setUtime(Timestamp utime) {
		this.utime = utime;
	}

	public String getUuser() {
		return this.uuser;
	}

	public void setUuser(String uuser) {
		this.uuser = uuser;
	}

	public long getUversion() {
		return this.uversion;
	}

	public void setUversion(long uversion) {
		this.uversion = uversion;
	}

	
	
	
//	
//	public Set<Usergroupmember> getUsergroupmembers() {
//		return this.usergroupmembers;
//	}
//
//	public void setUsergroupmembers(Set<Usergroupmember> usergroupmembers) {
//		this.usergroupmembers = usergroupmembers;
//	}
//	
//	
//
//	public Usergroupmember addUsergroupmember(Usergroupmember usergroupmember) {
//		getUsergroupmembers().add(usergroupmember);
//		usergroupmember.setUsergroup(this);
//
//		return usergroupmember;
//	}
//
//	public Usergroupmember removeUsergroupmember(Usergroupmember usergroupmember) {
//		getUsergroupmembers().remove(usergroupmember);
//		usergroupmember.setUsergroup(null);
//
//		return usergroupmember;
//	}
	

	public Set<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(Set<Employee> employees) {
		this.employees = employees;
	}

	public Set<GmdTutorial> getGmdtutorials() {
		return gmdtutorials;
	}

	public void setGmdtutorials(Set<GmdTutorial> gmdtutorials) {
		this.gmdtutorials = gmdtutorials;
	}

//	public Set<GmdUsergrouptutorial> getGmdUsergrouptutorials() {
//		return this.gmdUsergrouptutorials;
//	}
//
//	public void setGmdUsergrouptutorials(Set<GmdUsergrouptutorial> gmdUsergrouptutorials) {
//		this.gmdUsergrouptutorials = gmdUsergrouptutorials;
//	}
//
//	public GmdUsergrouptutorial addGmdUsergrouptutorial(GmdUsergrouptutorial gmdUsergrouptutorial) {
//		getGmdUsergrouptutorials().add(gmdUsergrouptutorial);
//		gmdUsergrouptutorial.setUsergroup(this);
//
//		return gmdUsergrouptutorial;
//	}
//
//	public GmdUsergrouptutorial removeGmdUsergrouptutorial(GmdUsergrouptutorial gmdUsergrouptutorial) {
//		getGmdUsergrouptutorials().remove(gmdUsergrouptutorial);
//		gmdUsergrouptutorial.setUsergroup(null);
//
//		return gmdUsergrouptutorial;
//	}
	
	

}