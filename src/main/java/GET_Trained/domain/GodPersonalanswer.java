package GET_Trained.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the GOD_PERSONALANSWER database table.
 * 
 */
@Entity
@Table(name = "GOD_PERSONALANSWER")
@NamedQuery(name = "GodSelectedanswer.findAll", query = "SELECT g FROM GodPersonalanswer g")
public class GodPersonalanswer implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;

	// bi-directional many-to-one association to GodPersonaltutorial
	@ManyToOne
	@JoinColumn(name = "PERSONALTUTORIAL_FK")
	private GodPersonaltutorial godPersonaltutorial;

	// bi-directional many-to-one association to GodAnswer
	@ManyToOne
	@JoinColumn(name = "ANSWER_FK")
	private GodAnswer godAnswer;

	@Column(name = "SELECTED")
	private boolean selected;

	
	
	public GodPersonalanswer(GodPersonaltutorial godPersonaltutorial, GodAnswer godAnswer, boolean selected) {
		super();
		this.godPersonaltutorial = godPersonaltutorial;
		this.godAnswer = godAnswer;
		this.selected = selected;
	}

	public GodPersonalanswer() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public GodPersonaltutorial getGodPersonaltutorial() {
		return godPersonaltutorial;
	}

	public void setGodPersonaltutorial(GodPersonaltutorial godPersonaltutorial) {
		this.godPersonaltutorial = godPersonaltutorial;
	}

	public GodAnswer getGodAnswer() {
		return godAnswer;
	}

	public void setGodAnswer(GodAnswer godAnswer) {
		this.godAnswer = godAnswer;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	

}
