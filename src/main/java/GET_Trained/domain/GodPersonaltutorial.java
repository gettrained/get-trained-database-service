package GET_Trained.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The persistent class for the GOD_PERSONALTUTORIAL database table.
 * 
 */
@Entity
@Table(name = "GOD_PERSONALTUTORIAL")
@NamedQuery(name = "GodPersonaltutorial.findAll", query = "SELECT g FROM GodPersonaltutorial g")
public class GodPersonaltutorial implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;

	// bi-directional many-to-one association to GodTutorial
	@ManyToOne
	@JoinColumn(name = "TUTORIAL_FK")
	@JsonIgnore
	private GodTutorial godTutorial;

	// bi-directional many-to-one association to Employee
	@ManyToOne
	@JoinColumn(name = "EMPLOYEE_FK")
	@JsonIgnore
	private Employee employee;

	@Column(name = "STATE")
	private String state;

	@Column(name = "LASTSEEN")
	private String lastseen;

	@Column(name = "DATEFINISHED")
	private Date datefinished;

	@Column(name = "DATECREATED")
	private Date datecreated;

	@Column(name = "USEDTIME")
	private int usedtime;

	// bi-directional many-to-one association to GodSelectedanswer
	@OneToMany(mappedBy = "godPersonaltutorial")
	private Set<GodPersonalanswer> godSelectedanswers;

	
	


	public GodPersonaltutorial(GodTutorial godTutorial, Employee employee, String state, String lastseen,
			Date datecreated, int usedtime) {
		super();
		this.godTutorial = godTutorial;
		this.employee = employee;
		this.state = state;
		this.lastseen = lastseen;
		this.datecreated = datecreated;
		this.usedtime = usedtime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public GodTutorial getGodTutorial() {
		return godTutorial;
	}

	public void setGodTutorial(GodTutorial godTutorial) {
		this.godTutorial = godTutorial;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getLastseen() {
		return lastseen;
	}

	public void setLastseen(String lastseen) {
		this.lastseen = lastseen;
	}

	public Date getDatefinished() {
		return datefinished;
	}

	public void setDatefinished(Date datefinished) {
		this.datefinished = datefinished;
	}

	public Date getDatecreated() {
		return datecreated;
	}

	public void setDatecreated(Date datecreated) {
		this.datecreated = datecreated;
	}

	public int getUsedtime() {
		return usedtime;
	}

	public void setUsedtime(int usedtime) {
		this.usedtime = usedtime;
	}

	public Set<GodPersonalanswer> getGodSelectedanswers() {
		return this.godSelectedanswers;
	}

	public void setGodSelectedanswers(Set<GodPersonalanswer> godSelectedanswers) {
		this.godSelectedanswers = godSelectedanswers;
	}

	public GodPersonalanswer addGodAnswer(GodPersonalanswer godSelectedanswer) {
		getGodSelectedanswers().add(godSelectedanswer);
		godSelectedanswer.setGodPersonaltutorial(this);

		return godSelectedanswer;
	}

	public GodPersonalanswer removeGodAnswer(GodPersonalanswer godSelectedanswer) {
		getGodSelectedanswers().remove(godSelectedanswer);
		godSelectedanswer.setGodPersonaltutorial(null);

		return godSelectedanswer;
	}

	public GodPersonaltutorial() {
	}

	public GodPersonaltutorial( GodTutorial godTutorial, Employee employee, String state, String lastseen,
			Date datefinished, Date datecreated, int usedtime, Set<GodPersonalanswer> godSelectedanswers) {
		super();
	
		this.godTutorial = godTutorial;
		this.employee = employee;
		this.state = state;
		this.lastseen = lastseen;
		this.datefinished = datefinished;
		this.datecreated = datecreated;
		this.usedtime = usedtime;
		this.godSelectedanswers = godSelectedanswers;
	}

	

}
