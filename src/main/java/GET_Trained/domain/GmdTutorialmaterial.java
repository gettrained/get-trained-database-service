package GET_Trained.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the GMD_TUTORIALMATERIAL database table.
 * 
 */
@Entity
@Table(name = "GMD_TUTORIALMATERIAL")
@NamedQuery(name = "GmdTutorialmaterial.findAll", query = "SELECT g FROM GmdTutorialmaterial g")
public class GmdTutorialmaterial implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;

	// bi-directional many-to-one association to GmdTutorial
	@ManyToOne
	@JoinColumn(name = "TUTORIAL_FK")
	private GmdTutorial gmdTutorial;

	@Column(name = "[ORDER]")
	private int order;

	// bi-directional many-to-one association to GmdTutorialtype
	@ManyToOne
	@JoinColumn(name = "TYPE_FK")
	private GmdTutorialtype gmdTutorialtype;

	// bi-directional one-to-one association to GmdTUtorialvideo
	@OneToOne
	@JoinColumn(name = "VIDEO_FK")
	private GmdTutorialvideo gmdTutorialvideo;

	// bi-directional one-to-one association to GmdTutorialimgae
	@OneToOne
	@JoinColumn(name = "IMAGE_FK")
	private GmdTutorialimage gmdTutorialimage;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public GmdTutorial getGmdTutorial() {
		return gmdTutorial;
	}

	public void setGmdTutorial(GmdTutorial gmdTutorial) {
		this.gmdTutorial = gmdTutorial;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public GmdTutorialtype getGmdTutorialtype() {
		return gmdTutorialtype;
	}

	public void setGmdTutorialtype(GmdTutorialtype gmdTutorialtype) {
		this.gmdTutorialtype = gmdTutorialtype;
	}

	public GmdTutorialvideo getGmdTutorialvideo() {
		return gmdTutorialvideo;
	}

	public void setGmdTutorialvideo(GmdTutorialvideo gmdTutorialvideo) {
		this.gmdTutorialvideo = gmdTutorialvideo;
	}

	public GmdTutorialimage getGmdTutorialimage() {
		return gmdTutorialimage;
	}

	public void setGmdTutorialimage(GmdTutorialimage gmdTutorialimage) {
		this.gmdTutorialimage = gmdTutorialimage;
	}

}
