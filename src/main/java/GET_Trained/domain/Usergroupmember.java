package GET_Trained.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the USERGROUPMEMBER database table.
 * 
 */
@Entity
@Table(name = "USERGROUPMEMBER")
@NamedQuery(name = "Usergroupmember.findAll", query = "SELECT g FROM Usergroupmember g")
public class Usergroupmember {
	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;

	// bi-directional many-to-one association to Usergroup
	@ManyToOne
	@JoinColumn(name = "USERGROUP_FK")
	private Usergroup usergroup;

	// bi-directional many-to-one association to Employee
	@ManyToOne
	@JoinColumn(name = "EMPLOYEE_FK")
	private Employee employee;

	@Column(name = "ISADUSERGROUPMEMBER")
	private int isadusergroupmember;

	@Column(name = "UTIME")
	private Date utime;

	@Column(name = "UVERSION")
	private int uversion;

	@Column(name = "UUSER")
	private String uuser;

	@Column(name = "UDELETE")
	private int udelete;

	@Column(name = "USESSION")
	private int usession;

	public Usergroup getUsergroup() {
		return usergroup;
	}

	public void setUsergroup(Usergroup usergroup) {
		this.usergroup = usergroup;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public int getIsadusergroupmember() {
		return isadusergroupmember;
	}

	public void setIsadusergroupmember(int isadusergroupmember) {
		this.isadusergroupmember = isadusergroupmember;
	}

	public Date getUtime() {
		return utime;
	}

	public void setUtime(Date utime) {
		this.utime = utime;
	}

	public int getUversion() {
		return uversion;
	}

	public void setUversion(int uversion) {
		this.uversion = uversion;
	}

	public String getUuser() {
		return uuser;
	}

	public void setUuser(String uuser) {
		this.uuser = uuser;
	}

	public int getUdelete() {
		return udelete;
	}

	public void setUdelete(int udelete) {
		this.udelete = udelete;
	}

	public int getUsession() {
		return usession;
	}

	public void setUsession(int usession) {
		this.usession = usession;
	}

}
