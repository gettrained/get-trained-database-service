package GET_Trained;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import GET_Trained.domain.Employee;
import GET_Trained.domain.GmdAnswer;
import GET_Trained.domain.GmdQuestion;
import GET_Trained.domain.GmdTutorial;
import GET_Trained.domain.GmdTutorialimage;
import GET_Trained.domain.GmdTutorialmaterial;
import GET_Trained.domain.GmdTutorialtype;
import GET_Trained.domain.GmdTutorialvideo;
import GET_Trained.domain.GodAnswer;
import GET_Trained.domain.GodPersonalanswer;
import GET_Trained.domain.GodPersonaltutorial;
import GET_Trained.domain.GodQuestion;
import GET_Trained.domain.GodTutorial;
import GET_Trained.domain.GodTutorialimage;
import GET_Trained.domain.GodTutorialmaterial;
import GET_Trained.domain.GodTutorialtype;
import GET_Trained.domain.GodTutorialvideo;
import GET_Trained.domain.Usergroup;
import GET_Trained.repositories.EmployeeRepo;
import GET_Trained.repositories.GmdTutorialRepo;
import GET_Trained.repositories.GodAnswerRepo;
import GET_Trained.repositories.GodPersonalanswerRepo;
import GET_Trained.repositories.GodPersonaltutorialRepo;
import GET_Trained.repositories.GodQuestionRepo;
import GET_Trained.repositories.GodTutorialImageRepo;
import GET_Trained.repositories.GodTutorialRepo;
import GET_Trained.repositories.GodTutorialmaterialRepo;
import GET_Trained.repositories.GodTutorialtypeRepo;
import GET_Trained.repositories.GodTutorialvideoRepo;
import GET_Trained.restControllers.TutorialRestController;

@EnableAutoConfiguration
@ComponentScan
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { TutorialRestController.class })
@SpringBootTest
@AutoConfigureMockMvc
@WebAppConfiguration
public class RestServiceTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private GodTutorialRepo godTutorialRepo;

	@MockBean
	private EmployeeRepo employeeRepo;

	@MockBean
	private GmdTutorialRepo gmdTutorialRepo;

	@MockBean
	private GodQuestionRepo godQuestionRepo;

	@MockBean
	private GodAnswerRepo godAnswerRepo;

	@MockBean
	private GodPersonaltutorialRepo godPersonaltutorialRepo;

	@MockBean
	private GodPersonalanswerRepo godPersonalanswerRepo;

	@MockBean
	private GodTutorialtypeRepo godTutorialtypeRepo;

	@MockBean
	private GodTutorialImageRepo godTutorialImageRepo;

	@MockBean
	private GodTutorialmaterialRepo godTutorialmaterialRepo;

	@MockBean
	private GodTutorialvideoRepo godTutorialvideoRepo;

	@Test
	public void getTutorialsSucceswithExistingTutorials() throws Exception {
		// The Tutorials are in the GodTutorial Table and not not have to get copied
		// over from GmdTutorial

		final GmdTutorial gmdTutorial = new GmdTutorial();
		gmdTutorial.setIdentificationcode(1234);

		final Set<GmdTutorial> gmdTutorials = new HashSet<>();
		gmdTutorials.add(gmdTutorial);

		final Usergroup usergroup = new Usergroup();
		usergroup.setGmdtutorials(gmdTutorials);

		final Set<Usergroup> usergroups = new HashSet<>();
		usergroups.add(usergroup);

		final Employee employee = new Employee();
		employee.setUsergroups(usergroups);

		Mockito.when(employeeRepo.findOne(anyInt())).thenReturn(employee);

		final GodTutorial godTutorial = new GodTutorial();
		godTutorial.setId(1);
		godTutorial.setSuccesspercentage(80);
		godTutorial.setColor("ColorString");
		godTutorial.setTutorialname("Mock Tut");

		Mockito.when(godTutorialRepo.findByIDCode(anyInt())).thenReturn(godTutorial);

		final String response = mockMvc.perform(get("/employee/getTutorials/1")).andExpect(status().isOk()).andReturn()
				.getResponse().getContentAsString();
		assertThat(response).isNotNull();
		assertThat(response).isNotEmpty();
		assertThat(response).contains(godTutorial.getTutorialname());

	}

	@Test
	public void getTutorialsSucceswithNoExistingTutorials() throws Exception {
		// There are no Tutorials stored in GodTutorial and they need to get Copied over
		// from GmdTutorial

		final GmdTutorial gmdTutorial = new GmdTutorial();
		gmdTutorial.setIdentificationcode(1234);

		final Set<GmdTutorial> gmdTutorials = new HashSet<>();
		gmdTutorials.add(gmdTutorial);

		final Usergroup usergroup = new Usergroup();
		usergroup.setGmdtutorials(gmdTutorials);

		final Set<Usergroup> usergroups = new HashSet<>();
		usergroups.add(usergroup);

		final Employee employee = new Employee();
		employee.setUsergroups(usergroups);

		Mockito.when(employeeRepo.findOne(anyInt())).thenReturn(employee);

		Mockito.when(godTutorialRepo.findByIDCode(anyInt())).thenReturn(null);

		final GodTutorial godTutorial = new GodTutorial();
		godTutorial.setId(1);
		godTutorial.setSuccesspercentage(80);
		godTutorial.setColor("ColorString");
		godTutorial.setTutorialname("Mock Tut");

		Mockito.when(godTutorialRepo.save(any(GodTutorial.class))).thenReturn(godTutorial);

		final String response = mockMvc.perform(get("/employee/getTutorials/1")).andExpect(status().isOk()).andReturn()
				.getResponse().getContentAsString();
		assertThat(response).isNotNull();
		assertThat(response).isNotEmpty();
		assertThat(response).contains(godTutorial.getTutorialname());

	}

	@Test
	public void getTutorialsNotFound() throws Exception {
		// The requested Employee with the specified name is not in Employee
		Mockito.when(employeeRepo.findOne(anyInt())).thenReturn(null);

		mockMvc.perform(get("/employee/getTutorials/1")).andExpect(status().isNoContent());
	}

	@Test
	public void getTutorialsBadRequest() throws Exception {
		// Request with invalid PathParameter
		mockMvc.perform(get("/employee/getTutorials/thisisnotastring")).andExpect(status().isBadRequest());
		mockMvc.perform(get("/employee/getTutorials/null")).andExpect(status().isBadRequest());
		mockMvc.perform(get("/employee/getTutorials/\"test")).andExpect(status().isBadRequest());
	}

	@Test
	public void getPersTutorialsSucces() throws Exception {
		// There is a PersonalTutorial for the specified id available.
		final GodTutorial godTutorial = new GodTutorial();
		godTutorial.setId(1);

		final GodPersonaltutorial godPersonaltutorial = new GodPersonaltutorial();
		godPersonaltutorial.setId(1);
		godPersonaltutorial.setState("done");
		godPersonaltutorial.setGodTutorial(godTutorial);
		godPersonaltutorial.setLastseen("Mock lastseen");
		godPersonaltutorial.setDatecreated(new Date());
		godPersonaltutorial.setDatefinished(new Date());
		godPersonaltutorial.setUsedtime(20);

		final Set<GodPersonaltutorial> godPersonaltutorials = new HashSet<>();
		godPersonaltutorials.add(godPersonaltutorial);

		final Employee employee = new Employee();
		employee.setGodPersonaltutorials(godPersonaltutorials);

		Mockito.when(employeeRepo.findOne(anyInt())).thenReturn(employee);

		final String response = mockMvc.perform(get("/employee/getPersTutorials/1")).andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString();
		assertThat(response).isNotNull();
		assertThat(response).isNotEmpty();
		assertThat(response).contains(godPersonaltutorial.getLastseen());

	}

	@Test
	public void getPersTutorialsNotFound() throws Exception {
		Mockito.when(employeeRepo.findOne(anyInt())).thenReturn(null);
		mockMvc.perform(get("/employee/getTutorials/200")).andExpect(status().isNoContent());
	}

	@Test
	public void getPersTutorialsBadRequest() throws Exception {
		// Request with invalid PathParameter
		mockMvc.perform(get("/employee/getPersTutorials//thisisnotastring")).andExpect(status().isBadRequest());
		mockMvc.perform(get("/employee/getPersTutorials//null")).andExpect(status().isBadRequest());
		mockMvc.perform(get("/employee/getPersTutorials//\"test")).andExpect(status().isBadRequest());
	}

	@Test
	public void getEmployeeSuccess() throws Exception {
		Employee employee = new Employee();
		employee.setFirstname("Anna");
		employee.setLastname("Schuster");
		employee.setId(1);
		Mockito.when(employeeRepo.findOne(anyInt())).thenReturn(employee);

		final String response = mockMvc.perform(get("/getEmployee/1")).andExpect(status().isOk()).andReturn()
				.getResponse().getContentAsString();
		assertThat(response).isNotNull();
		assertThat(response).isNotEmpty();
		assertThat(response).contains(employee.getFirstname());
		assertThat(response).contains(employee.getId() + "");
		assertThat(response).contains(employee.getLastname());

	}

	@Test
	public void getEmployeeNotFound() throws Exception {
		Mockito.when(employeeRepo.findOne(anyInt())).thenReturn(null);

		mockMvc.perform(get("/getEmployee/1")).andExpect(status().isNoContent());
	}

	@Test
	public void getEmployeeBadRequest() throws Exception {
		// Request with invalid PathParameter
		mockMvc.perform(get("/getEmployee/thisisnotanumber")).andExpect(status().isBadRequest());
		mockMvc.perform(get("/getEmployee/null")).andExpect(status().isBadRequest());
		mockMvc.perform(get("/getEmployee/\"test")).andExpect(status().isBadRequest());
	}

	@Test
	public void getQuestionsSuccesswithExistingQuestions() throws Exception {
		// Es sind bereits God_Qestions in der Datenbank vorhanden.
		final GodAnswer godAnswer = new GodAnswer();
		godAnswer.setId(1);
		godAnswer.setCorrect(true);
		godAnswer.setAnswertext("Mock Answer");

		final Set<GodAnswer> godAnswers = new HashSet<>();
		godAnswers.add(godAnswer);

		final GodQuestion godQuestion = new GodQuestion();
		godQuestion.setText("Mock Text");
		godQuestion.setId(1);
		godQuestion.setOrder(1);
		byte[] image = new byte[1];
		godQuestion.setImage(image);
		godQuestion.setGodAnswers(godAnswers);

		final Set<GodQuestion> godQuestions = new HashSet<>();
		godQuestions.add(godQuestion);
		final GodTutorial godTutorial = new GodTutorial();
		godTutorial.setGodQuestions(godQuestions);
		Mockito.when(godTutorialRepo.findOne(anyInt())).thenReturn(godTutorial);

		final String response = mockMvc.perform(get("/tutorial/questions/1")).andExpect(status().isOk()).andReturn()
				.getResponse().getContentAsString();
		assertThat(response).isNotNull();
		assertThat(response).isNotEmpty();
		assertThat(response).contains(godAnswer.getAnswertext());
		assertThat(response).contains(godQuestion.getText());

	}

	@Test
	public void getQuestionsSuccesswithNoExistingQuestions() throws Exception {
		// Es noch keine God_Qestions in der Datenbank vorhanden.

		final Set<GodQuestion> godQuestions = new HashSet<>();

		final GodTutorial godTutorial = new GodTutorial();
		godTutorial.setIdentificationcode(1);
		godTutorial.setGodQuestions(godQuestions);

		Mockito.when(godTutorialRepo.findOne(anyInt())).thenReturn(godTutorial);

		final GmdAnswer gmdAnswer = new GmdAnswer();
		gmdAnswer.setAnswertext("Mock Answer");
		gmdAnswer.setCorrect(true);
		gmdAnswer.setId(1);

		final Set<GmdAnswer> gmdAnswers = new HashSet<>();
		gmdAnswers.add(gmdAnswer);

		final GmdQuestion gmdQuestion = new GmdQuestion();
		gmdQuestion.setId(1);
		gmdQuestion.setText("Mock Text");
		gmdQuestion.setOrder(1);
		gmdQuestion.setImage(new byte[1]);
		gmdQuestion.setGmdAnswers(gmdAnswers);

		final Set<GmdQuestion> gmdQuestions = new HashSet<>();
		gmdQuestions.add(gmdQuestion);

		final GmdTutorial gmdTutorial = new GmdTutorial();
		gmdTutorial.setGmdQuestions(gmdQuestions);

		Mockito.when(gmdTutorialRepo.findByIDCode(anyInt())).thenReturn(gmdTutorial);

		Mockito.when(godQuestionRepo.save(any(GodQuestion.class))).thenReturn(new GodQuestion(gmdQuestion.getImage(),
				gmdQuestion.getText(), gmdQuestion.getOrder(), godTutorial, null));

		Mockito.when(godAnswerRepo.save(any(GodAnswer.class)))
				.thenReturn(new GodAnswer(gmdAnswer.getCorrect(), gmdAnswer.getAnswertext(), null));

		final String response = mockMvc.perform(get("/tutorial/questions/1")).andExpect(status().isOk()).andReturn()
				.getResponse().getContentAsString();
		assertThat(response).isNotNull();
		assertThat(response).isNotEmpty();
		assertThat(response).contains(gmdAnswer.getAnswertext());
		assertThat(response).contains(gmdQuestion.getText());

	}

	@Test
	public void getQuestionsNotFound() throws Exception {
		Mockito.when(employeeRepo.findOne(anyInt())).thenReturn(null);

		mockMvc.perform(get("/tutorial/questions/1")).andExpect(status().isNoContent());
	}

	@Test
	public void getQuestionsBadRequest() throws Exception {
		// Request with invalid PathParameter
		mockMvc.perform(get("/tutorial/questions/thisisnotanumber")).andExpect(status().isBadRequest());
		mockMvc.perform(get("/tutorial/questions/null")).andExpect(status().isBadRequest());
		mockMvc.perform(get("/tutorial/questions/\"test")).andExpect(status().isBadRequest());
	}

	@Test
	public void addPersonalAnswersSuccessNoExisting() throws Exception {
		final String postBody = "{\"personalanswers\":[{\"personaltutorialID\":18,\"answerID\":453,\"selected\":true},{\"personaltutorialID\":18,\"answerID\":454,\"selected\":true},{\"personaltutorialID\":18,\"answerID\":455,\"selected\":false},{\"personaltutorialID\":18,\"answerID\":456,\"selected\":false}]}";

		Mockito.when(godPersonaltutorialRepo.findOne(anyInt())).thenReturn(new GodPersonaltutorial());

		Mockito.when(godAnswerRepo.findOne(anyInt())).thenReturn(new GodAnswer());

		Mockito.when(godPersonalanswerRepo.findByPerstutandAnswerID(anyInt(), anyInt())).thenReturn(null);

		Mockito.when(godPersonalanswerRepo.save(any(GodPersonalanswer.class))).thenReturn(null);

		mockMvc.perform(post("/employee/personaltutorial/addpersonalanswer").contentType(MediaType.APPLICATION_JSON)
				.content(postBody)).andExpect(status().isCreated());

	}

	@Test
	public void addPersonalAnswersSuccessExisting() throws Exception {
		final String postBody = "{\"personalanswers\":[{\"personaltutorialID\":18,\"answerID\":453,\"selected\":true},{\"personaltutorialID\":18,\"answerID\":454,\"selected\":true},{\"personaltutorialID\":18,\"answerID\":455,\"selected\":false},{\"personaltutorialID\":18,\"answerID\":456,\"selected\":false}]}";

		Mockito.when(godPersonaltutorialRepo.findOne(anyInt())).thenReturn(new GodPersonaltutorial());

		Mockito.when(godAnswerRepo.findOne(anyInt())).thenReturn(new GodAnswer());

		Mockito.when(godPersonalanswerRepo.findByPerstutandAnswerID(anyInt(), anyInt()))
				.thenReturn(new GodPersonalanswer());

		Mockito.when(godPersonalanswerRepo.save(any(GodPersonalanswer.class))).thenReturn(null);

		mockMvc.perform(post("/employee/personaltutorial/addpersonalanswer").contentType(MediaType.APPLICATION_JSON)
				.content(postBody)).andExpect(status().isCreated());

	}

	@Test
	public void addPersonalAnswersinvalidPersonalTut() throws Exception {
		final String postBody = "{\"personalanswers\":[{\"personaltutorialID\":18,\"answerID\":453,\"selected\":true},{\"personaltutorialID\":18,\"answerID\":454,\"selected\":true},{\"personaltutorialID\":18,\"answerID\":455,\"selected\":false},{\"personaltutorialID\":18,\"answerID\":456,\"selected\":false}]}";

		Mockito.when(godPersonaltutorialRepo.findOne(anyInt())).thenReturn(null);

		Mockito.when(godAnswerRepo.findOne(anyInt())).thenReturn(new GodAnswer());

		Mockito.when(godPersonalanswerRepo.findByPerstutandAnswerID(anyInt(), anyInt())).thenReturn(null);

		Mockito.when(godPersonalanswerRepo.save(any(GodPersonalanswer.class))).thenReturn(null);

		mockMvc.perform(post("/employee/personaltutorial/addpersonalanswer").contentType(MediaType.APPLICATION_JSON)
				.content(postBody)).andExpect(status().isNotModified());

	}

	@Test
	public void addPersonalAnswersinvalidAnswer() throws Exception {
		final String postBody = "{\"personalanswers\":[{\"personaltutorialID\":18,\"answerID\":453,\"selected\":true},{\"personaltutorialID\":18,\"answerID\":454,\"selected\":true},{\"personaltutorialID\":18,\"answerID\":455,\"selected\":false},{\"personaltutorialID\":18,\"answerID\":456,\"selected\":false}]}";

		Mockito.when(godPersonaltutorialRepo.findOne(anyInt())).thenReturn(new GodPersonaltutorial());

		Mockito.when(godAnswerRepo.findOne(anyInt())).thenReturn(null);

		Mockito.when(godPersonalanswerRepo.findByPerstutandAnswerID(anyInt(), anyInt())).thenReturn(null);

		Mockito.when(godPersonalanswerRepo.save(any(GodPersonalanswer.class))).thenReturn(null);

		mockMvc.perform(post("/employee/personaltutorial/addpersonalanswer").contentType(MediaType.APPLICATION_JSON)
				.content(postBody)).andExpect(status().isNotModified());

	}

	@Test
	public void finishPersonalTutorialsSuccess() throws Exception {
		final String postBody = "{\"personalTutorialID\":8,\"usedtime\":25}";

		Mockito.when(godPersonaltutorialRepo.findOne(anyInt())).thenReturn(new GodPersonaltutorial());

		Mockito.when(godPersonaltutorialRepo.save(any(GodPersonaltutorial.class))).thenReturn(null);

		final String response = mockMvc
				.perform(post("/employee/personaltutorial/finish").contentType(MediaType.APPLICATION_JSON)
						.content(postBody))
				.andExpect(status().isNoContent()).andReturn().getResponse().getContentAsString();
		assertThat(response).isNotNull();
		assertThat(response).isNotEmpty();
		assertThat(response).contains("true");
	}

	@Test
	public void finishPersonalTutorialsinvalidPersTut() throws Exception {
		final String postBody = "{\"personalTutorialID\":8,\"usedtime\":25}";

		Mockito.when(godPersonaltutorialRepo.findOne(anyInt())).thenReturn(null);

		Mockito.when(godPersonaltutorialRepo.save(any(GodPersonaltutorial.class))).thenReturn(null);

		final String response = mockMvc
				.perform(post("/employee/personaltutorial/finish").contentType(MediaType.APPLICATION_JSON)
						.content(postBody))
				.andExpect(status().isNotModified()).andReturn().getResponse().getContentAsString();
		assertThat(response).isNotNull();
		assertThat(response).isNotEmpty();
		assertThat(response).contains("false");
	}

	@Test
	public void pausePersonalTutorialsSuccess() throws Exception {
		final String postBody = "{\"personalTutorialID\":8,\"usedtime\":25}";

		Mockito.when(godPersonaltutorialRepo.findOne(anyInt())).thenReturn(new GodPersonaltutorial());

		Mockito.when(godPersonaltutorialRepo.save(any(GodPersonaltutorial.class))).thenReturn(null);

		final String response = mockMvc
				.perform(post("/employee/personaltutorial/finish").contentType(MediaType.APPLICATION_JSON)
						.content(postBody))
				.andExpect(status().isNoContent()).andReturn().getResponse().getContentAsString();
		assertThat(response).isNotNull();
		assertThat(response).isNotEmpty();
		assertThat(response).contains("true");
	}

	@Test
	public void pausePersonalTutorialsinvalidPersTut() throws Exception {
		final String postBody = "{\"personalTutorialID\":8,\"usedtime\":25,\"lastseen\":\"test\"}";

		Mockito.when(godPersonaltutorialRepo.findOne(anyInt())).thenReturn(null);

		Mockito.when(godPersonaltutorialRepo.save(any(GodPersonaltutorial.class))).thenReturn(null);

		final String response = mockMvc
				.perform(post("/employee/personaltutorial/pause").contentType(MediaType.APPLICATION_JSON)
						.content(postBody))
				.andExpect(status().isNotModified()).andReturn().getResponse().getContentAsString();
		assertThat(response).isNotNull();
		assertThat(response).isNotEmpty();
		assertThat(response).contains("false");
	}

	@Test
	public void getResultSuccess() throws Exception {
		// Fill SelecedAnswers
		final Set<GodPersonalanswer> godSelectedanswers = new HashSet<>();

		// SelecedAnswer1 with GodAnswerId 1
		final GodAnswer godAnswerForPersAnswer1 = new GodAnswer();
		godAnswerForPersAnswer1.setId(1);
		final GodPersonalanswer godPersonalanswer1 = new GodPersonalanswer();
		godPersonalanswer1.setGodAnswer(godAnswerForPersAnswer1);
		godPersonalanswer1.setSelected(true);
		godSelectedanswers.add(godPersonalanswer1);

		// SelecedAnswer2 with GodAnswerId 2
		final GodAnswer godAnswerForPersAnswer2 = new GodAnswer();
		godAnswerForPersAnswer2.setId(2);
		final GodPersonalanswer godPersonalanswer2 = new GodPersonalanswer();
		godPersonalanswer2.setGodAnswer(godAnswerForPersAnswer2);
		godPersonalanswer2.setSelected(true);
		godSelectedanswers.add(godPersonalanswer2);

		// SelecedAnswer3 with GodAnswerId 3
		final GodAnswer godAnswerForPersAnswer3 = new GodAnswer();
		godAnswerForPersAnswer3.setId(3);
		final GodPersonalanswer godPersonalanswer3 = new GodPersonalanswer();
		godPersonalanswer3.setGodAnswer(godAnswerForPersAnswer3);
		godPersonalanswer3.setSelected(false);
		godSelectedanswers.add(godPersonalanswer3);

		// SelecedAnswer4 with GodAnswerId 4
		final GodAnswer godAnswerForPersAnswer4 = new GodAnswer();
		godAnswerForPersAnswer4.setId(4);
		final GodPersonalanswer godPersonalanswer4 = new GodPersonalanswer();
		godPersonalanswer4.setGodAnswer(godAnswerForPersAnswer4);
		godPersonalanswer4.setSelected(false);
		godSelectedanswers.add(godPersonalanswer4);

		// SelecedAnswer5 with GodAnswerId 5
		final GodAnswer godAnswerForPersAnswer5 = new GodAnswer();
		godAnswerForPersAnswer5.setId(3);
		final GodPersonalanswer godPersonalanswer5 = new GodPersonalanswer();
		godPersonalanswer5.setGodAnswer(godAnswerForPersAnswer5);
		godPersonalanswer5.setSelected(true);
		godSelectedanswers.add(godPersonalanswer5);

		// SelecedAnswer6 with GodAnswerId 6
		final GodAnswer godAnswerForPersAnswer6 = new GodAnswer();
		godAnswerForPersAnswer6.setId(6);
		final GodPersonalanswer godPersonalanswer6 = new GodPersonalanswer();
		godPersonalanswer6.setGodAnswer(godAnswerForPersAnswer6);
		godPersonalanswer6.setSelected(false);
		godSelectedanswers.add(godPersonalanswer6);

		// Fill GodTutorials
		final Set<GodQuestion> godQuestions = new HashSet<>();

		final GodTutorial godTutorial = new GodTutorial();
		godTutorial.setGodQuestions(godQuestions);

		// GodQuestion1 with GodAnswers with the ids 1-2
		// Everything matches with the PersonalAnswers
		final GodQuestion godQuestion1 = new GodQuestion();
		// Answer 1
		final GodAnswer godAnswer1ForgodQuestion1 = new GodAnswer();
		godAnswer1ForgodQuestion1.setId(1);
		godAnswer1ForgodQuestion1.setCorrect(true);
		godQuestion1.addGodAnswer(godAnswer1ForgodQuestion1);
		// Answer 2
		final GodAnswer godAnswer2ForgodQuestion1 = new GodAnswer();
		godAnswer2ForgodQuestion1.setId(2);
		godAnswer2ForgodQuestion1.setCorrect(true);
		godQuestion1.addGodAnswer(godAnswer2ForgodQuestion1);

		godQuestions.add(godQuestion1);

		// GodQuestion2 with GodAnswers with the ids 3-4
		// Nothing matches with the PersonalAnswers
		final GodQuestion godQuestion2 = new GodQuestion();
		// Answer 1
		final GodAnswer godAnswer1ForgodQuestion2 = new GodAnswer();
		godAnswer1ForgodQuestion2.setId(5);
		godAnswer1ForgodQuestion2.setCorrect(true);
		godQuestion2.addGodAnswer(godAnswer1ForgodQuestion2);
		// Answer 2
		final GodAnswer godAnswer2ForgodQuestion2 = new GodAnswer();
		godAnswer2ForgodQuestion2.setId(6);
		godAnswer2ForgodQuestion2.setCorrect(true);
		godQuestion2.addGodAnswer(godAnswer2ForgodQuestion2);

		godQuestions.add(godQuestion2);

		// GodQuestion2 with GodAnswers with the ids 3-4
		// Some match with the corresponding PersonalAnswers
		final GodQuestion godQuestion3 = new GodQuestion();
		// Answer 1
		final GodAnswer godAnswer1ForgodQuestion3 = new GodAnswer();
		godAnswer1ForgodQuestion3.setId(5);
		godAnswer1ForgodQuestion3.setCorrect(true);
		godQuestion3.addGodAnswer(godAnswer1ForgodQuestion3);
		// Answer 2
		final GodAnswer godAnswer2ForgodQuestion3 = new GodAnswer();
		godAnswer2ForgodQuestion3.setId(6);
		godAnswer2ForgodQuestion3.setCorrect(true);
		godQuestion3.addGodAnswer(godAnswer2ForgodQuestion3);

		godQuestions.add(godQuestion3);

		final GodPersonaltutorial godPersonaltutorial = new GodPersonaltutorial();
		godPersonaltutorial.setGodSelectedanswers(godSelectedanswers);
		godPersonaltutorial.setGodTutorial(godTutorial);

		Mockito.when(godPersonaltutorialRepo.findOne(anyInt())).thenReturn(godPersonaltutorial);
		final String response = mockMvc.perform(get("/employee/personaltutorial/getResult/1"))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
		assertThat(response).isNotNull();
		assertThat(response).isNotEmpty();
		assertThat(response).contains("\"correctAnswers\":1");
		assertThat(response).contains("\"totalAnswers\":3");
	}

	@Test
	public void getResultNotFound() throws Exception {
		Mockito.when(godPersonaltutorialRepo.findOne(anyInt())).thenReturn(null);

		mockMvc.perform(get("/employee/personaltutorial/getResult/1")).andExpect(status().isNoContent());
	}

	@Test
	public void getResultBadRequest() throws Exception {
		// Request with invalid PathParameter
		mockMvc.perform(get("/getEmployee/thisisnotanumber")).andExpect(status().isBadRequest());
		mockMvc.perform(get("/getEmployee/null")).andExpect(status().isBadRequest());
		mockMvc.perform(get("/getEmployee/\"test")).andExpect(status().isBadRequest());
	}

	@Test
	public void getTutorialSuccess() throws Exception {
		final GodTutorial godTutorial = new GodTutorial();
		godTutorial.setId(1);
		godTutorial.setSuccesspercentage(100);
		godTutorial.setColor("colorstring");
		godTutorial.setTutorialname("Mock name");
		Mockito.when(godTutorialRepo.findOne(anyInt())).thenReturn(godTutorial);

		final String response = mockMvc.perform(get("/getTutorial/1")).andExpect(status().isOk()).andReturn()
				.getResponse().getContentAsString();
		assertThat(response).isNotNull();
		assertThat(response).isNotEmpty();
		assertThat(response).contains(godTutorial.getTutorialname());
		assertThat(response).contains(godTutorial.getColor());

	}

	@Test
	public void getTutorialNotFound() throws Exception {
		Mockito.when(godPersonaltutorialRepo.findOne(anyInt())).thenReturn(null);

		mockMvc.perform(get("/getTutorial/1")).andExpect(status().isNoContent());
	}

	@Test
	public void getTutorialBadRequest() throws Exception {
		// Request with invalid PathParameter
		mockMvc.perform(get("/getTutorial/thisisnotanumber")).andExpect(status().isBadRequest());
		mockMvc.perform(get("/getTutorial/null")).andExpect(status().isBadRequest());
		mockMvc.perform(get("/getTutorial/\"test")).andExpect(status().isBadRequest());
	}

	@Test
	public void getMaterialsSucceswithExistingMaterials() throws Exception {
		// The Tutorialmaterials are in the GodTutorialMaterial Table and not not have
		// to get copied
		// over from GmdTutorialmaterial

		final GodTutorial godTutorial = new GodTutorial();

		final Set<GodTutorialmaterial> godTutorialmaterials = new HashSet<>();

		// Material with image
		final GodTutorialmaterial godTutorialmaterialImage = new GodTutorialmaterial();
		final GodTutorialimage godTutorialimage = new GodTutorialimage();
		godTutorialimage.setId(1);

		godTutorialmaterialImage.setGodTutorialimage(godTutorialimage);
		godTutorialmaterialImage.setId(1);
		godTutorialmaterialImage.setGodTutorialtype(new GodTutorialtype("image"));
		godTutorialmaterialImage.setOrder(1);
		godTutorialmaterialImage.setGodTutorial(new GodTutorial());

		godTutorialmaterials.add(godTutorialmaterialImage);

		// Material with video
		final GodTutorialmaterial godTutorialmaterialVideo = new GodTutorialmaterial();
		final GodTutorialvideo godTutorialvideo = new GodTutorialvideo();
		godTutorialvideo.setId(1);

		godTutorialmaterialVideo.setGodTutorialvideo(godTutorialvideo);
		godTutorialmaterialVideo.setId(2);
		godTutorialmaterialVideo.setGodTutorialtype(new GodTutorialtype("video"));
		godTutorialmaterialVideo.setOrder(2);
		godTutorialmaterialVideo.setGodTutorial(new GodTutorial());

		godTutorialmaterials.add(godTutorialmaterialVideo);
		godTutorial.setGodTutorialmaterials(godTutorialmaterials);

		Mockito.when(godTutorialRepo.findOne(anyInt())).thenReturn(godTutorial);

		final String response = mockMvc.perform(get("/tutorial/material/1")).andExpect(status().isOk()).andReturn()
				.getResponse().getContentAsString();
		assertThat(response).isNotNull();
		assertThat(response).isNotEmpty();
		assertThat(response).contains("image");
		assertThat(response).contains("video");

	}

	@Test
	public void getMaterialsSucceswithNoExistingMaterialsImage() throws Exception {
		// There are no Materials stored in GodTutorialMaterial and they need to get
		// Copied over
		// from GmdTutorialmaterial

		final Set<GmdTutorialmaterial> gmdTutorialmaterials = new HashSet<>();

		final GmdTutorialmaterial gmdTutorialmaterial = new GmdTutorialmaterial();

		final GmdTutorialimage gmdTutorialimage = new GmdTutorialimage();
		gmdTutorialimage.setText("Mock Text");
		gmdTutorialimage.setId(1);

		final GmdTutorialtype gmdTutorialtype = new GmdTutorialtype();
		gmdTutorialtype.setType("image");

		gmdTutorialmaterial.setGmdTutorialtype(gmdTutorialtype);
		gmdTutorialmaterial.setId(1);
		gmdTutorialmaterial.setOrder(1);
		gmdTutorialmaterial.setGmdTutorialimage(gmdTutorialimage);

		gmdTutorialmaterials.add(gmdTutorialmaterial);

		final GodTutorialmaterial godTutorialmaterial = new GodTutorialmaterial();
		godTutorialmaterial.setId(gmdTutorialmaterial.getId());
		godTutorialmaterial.setOrder(gmdTutorialmaterial.getOrder());

		final GmdTutorial gmdTutorial = new GmdTutorial();
		gmdTutorial.setGmdTutorialmaterials(gmdTutorialmaterials);

		Mockito.when(godTutorialmaterialRepo.save(any(GodTutorialmaterial.class))).thenReturn(godTutorialmaterial);

		final GodTutorialimage godTutorialimage = new GodTutorialimage();
		godTutorialimage.setId(gmdTutorialimage.getId());
		godTutorialimage.setText(gmdTutorialimage.getText());

		Mockito.when(godTutorialImageRepo.save(any(GodTutorialimage.class))).thenReturn(godTutorialimage);

		Mockito.when(godTutorialtypeRepo.findBytype(anyString())).thenReturn(null);

		Mockito.when(godTutorialtypeRepo.save(any(GodTutorialtype.class))).thenReturn(new GodTutorialtype("image"));

		Mockito.when(gmdTutorialRepo.findByIDCode(anyInt())).thenReturn(gmdTutorial);

		final GodTutorial godTutorial = new GodTutorial();
		final Set<GodTutorialmaterial> godTutorialmaterials = new HashSet<>();
		godTutorial.setGodTutorialmaterials(godTutorialmaterials);

		Mockito.when(godTutorialRepo.findOne(anyInt())).thenReturn(godTutorial);

		final String response = mockMvc.perform(get("/tutorial/material/1")).andExpect(status().isOk()).andReturn()
				.getResponse().getContentAsString();
		assertThat(response).isNotNull();
		assertThat(response).isNotEmpty();
		assertThat(response).contains("image");

	}

	@Test
	public void getMaterialsSucceswithNoExistingMaterialsVideo() throws Exception {
		// There are no Materials stored in GodTutorialMaterial and they need to get
		// Copied over
		// from GmdTutorialmaterial

		final Set<GmdTutorialmaterial> gmdTutorialmaterials = new HashSet<>();

		final GmdTutorialmaterial gmdTutorialmaterial = new GmdTutorialmaterial();

		final GmdTutorialvideo gmdTutorialvideo = new GmdTutorialvideo();
		gmdTutorialvideo.setId(1);

		final GmdTutorialtype gmdTutorialtype = new GmdTutorialtype();
		gmdTutorialtype.setType("video");

		gmdTutorialmaterial.setGmdTutorialtype(gmdTutorialtype);
		gmdTutorialmaterial.setId(1);
		gmdTutorialmaterial.setOrder(1);
		gmdTutorialmaterial.setGmdTutorialvideo(gmdTutorialvideo);

		gmdTutorialmaterials.add(gmdTutorialmaterial);

		final GodTutorialmaterial godTutorialmaterial = new GodTutorialmaterial();
		godTutorialmaterial.setId(gmdTutorialmaterial.getId());

		final GmdTutorial gmdTutorial = new GmdTutorial();
		gmdTutorial.setGmdTutorialmaterials(gmdTutorialmaterials);

		Mockito.when(godTutorialmaterialRepo.save(any(GodTutorialmaterial.class))).thenReturn(godTutorialmaterial);

		final GodTutorialvideo godTutorialvideo = new GodTutorialvideo();
		gmdTutorialvideo.setId(gmdTutorialvideo.getId());

		Mockito.when(godTutorialvideoRepo.save(any(GodTutorialvideo.class))).thenReturn(godTutorialvideo);

		Mockito.when(godTutorialtypeRepo.findBytype(anyString())).thenReturn(null);

		Mockito.when(godTutorialtypeRepo.save(any(GodTutorialtype.class))).thenReturn(new GodTutorialtype("video"));

		Mockito.when(gmdTutorialRepo.findByIDCode(anyInt())).thenReturn(gmdTutorial);

		final GodTutorial godTutorial = new GodTutorial();
		final Set<GodTutorialmaterial> godTutorialmaterials = new HashSet<>();
		godTutorial.setGodTutorialmaterials(godTutorialmaterials);

		Mockito.when(godTutorialRepo.findOne(anyInt())).thenReturn(godTutorial);

		final String response = mockMvc.perform(get("/tutorial/material/1")).andExpect(status().isOk()).andReturn()
				.getResponse().getContentAsString();
		assertThat(response).isNotNull();
		assertThat(response).isNotEmpty();
		assertThat(response).contains("video");

	}

	@Test
	public void getMaterialsNotFound() throws Exception {
		// The requested Employee with the specified name is not in Employee
		Mockito.when(godTutorialRepo.findOne(anyInt())).thenReturn(null);

		mockMvc.perform(get("/tutorial/material/1")).andExpect(status().isNoContent());
	}

	@Test
	public void getMaterialsBadRequest() throws Exception {
		// Request with invalid PathParameter
		mockMvc.perform(get("/tutorial/material/thisisnotastring")).andExpect(status().isBadRequest());
		mockMvc.perform(get("/tutorial/material/null")).andExpect(status().isBadRequest());
		mockMvc.perform(get("/tutorial/material/\"test")).andExpect(status().isBadRequest());
	}

	@Test
	public void getMaterialImageSuccess() throws Exception {
		final GodTutorialmaterial godTutorialmaterial = new GodTutorialmaterial();
		final GodTutorialimage godTutorialimage = new GodTutorialimage();
		godTutorialimage.setId(1);
		final byte[] bytearr = new byte[10];
		godTutorialimage.setImage(bytearr);
		godTutorialimage.setText("Mock Text");
		godTutorialmaterial.setId(1);
		godTutorialmaterial.setGodTutorialimage(godTutorialimage);
		Mockito.when(godTutorialmaterialRepo.findOne(anyInt())).thenReturn(godTutorialmaterial);

		final String response = mockMvc.perform(get("/tutorial/material/image/1")).andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString();
		assertThat(response).isNotNull();
		assertThat(response).isNotEmpty();
		assertThat(response).contains("Mock Text");
	}

	@Test
	public void getMaterialImageNotFound() throws Exception {
		// The requested Employee with the specified name is not in Employee
		Mockito.when(godTutorialRepo.findOne(anyInt())).thenReturn(null);

		mockMvc.perform(get("/tutorial/material/image/1")).andExpect(status().isNoContent());
	}

	@Test
	public void getMaterialImageBadRequest() throws Exception {
		// Request with invalid PathParameter
		mockMvc.perform(get("/tutorial/material/image/thisisnotastring")).andExpect(status().isBadRequest());
		mockMvc.perform(get("/tutorial/material/image/null")).andExpect(status().isBadRequest());
		mockMvc.perform(get("/tutorial/material/image/\"test")).andExpect(status().isBadRequest());
	}

	@Test
	public void getMaterialVideoSuccess() throws Exception {
		final GodTutorialmaterial godTutorialmaterial = new GodTutorialmaterial();
		final GodTutorialvideo godTutorialvideo = new GodTutorialvideo();
		godTutorialvideo.setId(1);
		godTutorialvideo.setVideo("http://www.ebookfrenzy.com/android_book/movie.mp4");
		godTutorialmaterial.setId(1);
		godTutorialmaterial.setGodTutorialvideo(godTutorialvideo);
		Mockito.when(godTutorialmaterialRepo.findOne(anyInt())).thenReturn(godTutorialmaterial);

		final String response = mockMvc.perform(get("/tutorial/material/video/1")).andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString();
		assertThat(response).isNotNull();
		assertThat(response).isNotEmpty();
		assertThat(response).contains("http://www.ebookfrenzy.com/android_book/movie.mp4");
	}

	@Test
	public void getMaterialVideoNotFound() throws Exception {
		Mockito.when(godTutorialRepo.findOne(anyInt())).thenReturn(null);

		mockMvc.perform(get("/tutorial/material/video/1")).andExpect(status().isNoContent());
	}

	@Test
	public void getMaterialVideoBadRequest() throws Exception {
		// Request with invalid PathParameter
		mockMvc.perform(get("/tutorial/material/video/thisisnotastring")).andExpect(status().isBadRequest());
		mockMvc.perform(get("/tutorial/material/video/null")).andExpect(status().isBadRequest());
		mockMvc.perform(get("/tutorial/material/video/\"test")).andExpect(status().isBadRequest());
	}

	@Test
	public void addPersonalTutorialSuccess() throws Exception{
		final String postBody = "{\"employeeID\":7,\"tutorialID\":110}";

		final GodPersonaltutorial godPersonaltutorial = new GodPersonaltutorial();
		godPersonaltutorial.setDatecreated(new Date());
		godPersonaltutorial.setId(1);
		godPersonaltutorial.setState("partial");
		godPersonaltutorial.setLastseen("0");
		godPersonaltutorial.setUsedtime(0);
		
		final Employee employee = new Employee();
		employee.setId(1);
		godPersonaltutorial.setEmployee(employee);
		
		final GodTutorial godTutorial = new GodTutorial();
		godTutorial.setId(1);
		godPersonaltutorial.setGodTutorial(godTutorial);
		Mockito.when(godPersonaltutorialRepo.save(any(GodPersonaltutorial.class))).thenReturn(godPersonaltutorial);
		Mockito.when(employeeRepo.findOne(anyInt())).thenReturn(new Employee());
		Mockito.when(godTutorialRepo.findOne(anyInt())).thenReturn(new GodTutorial());
		
		mockMvc.perform(post("/employee/addpersonaltutorial").contentType(MediaType.APPLICATION_JSON)
				.content(postBody)).andExpect(status().isCreated());
	}
	
	@Test
	public void addPersonalTutorialInvalidEmployee() throws Exception{
		final String postBody = "{\"employeeID\":7,\"tutorialID\":110}";

		Mockito.when(employeeRepo.findOne(anyInt())).thenReturn(null);
		Mockito.when(godTutorialRepo.findOne(anyInt())).thenReturn(new GodTutorial());
		
		mockMvc.perform(post("/employee/addpersonaltutorial").contentType(MediaType.APPLICATION_JSON)
				.content(postBody)).andExpect(status().isNotModified());
	}
	
	@Test
	public void addPersonalTutorialInvalidTut() throws Exception{
		final String postBody = "{\"employeeID\":7,\"tutorialID\":110}";

		Mockito.when(employeeRepo.findOne(anyInt())).thenReturn(new Employee());
		Mockito.when(godTutorialRepo.findOne(anyInt())).thenReturn(null);
		
		mockMvc.perform(post("/employee/addpersonaltutorial").contentType(MediaType.APPLICATION_JSON)
				.content(postBody)).andExpect(status().isNotModified());
	}

	@Test
	public void getPesonalAnswersSuccess() throws Exception{
		
		final GodPersonaltutorial godPersonaltutorial = new GodPersonaltutorial();
		final Set<GodPersonalanswer> godPersonalanswers = new HashSet<>();
		
		final GodPersonalanswer godPersonalanswer = new GodPersonalanswer();
		final GodAnswer godAnswer = new GodAnswer();
		godAnswer.setId(1);
		godPersonalanswer.setGodAnswer(godAnswer);
		godPersonalanswer.setSelected(true);
		godPersonalanswers.add(godPersonalanswer);
		godPersonaltutorial.setGodSelectedanswers(godPersonalanswers);
		
		Mockito.when(godPersonaltutorialRepo.findOne(anyInt())).thenReturn(godPersonaltutorial);

		
		final String response = mockMvc.perform(get("/employee/personalanswer/1")).andExpect(status().isOk()).andReturn()
				.getResponse().getContentAsString();
		assertThat(response).isNotNull();
		assertThat(response).isNotEmpty();
		assertThat(response).contains("true");
	}
	
	public void getPesonalAnswersNotFound() throws Exception{
		Mockito.when(godPersonaltutorialRepo.findOne(anyInt())).thenReturn(null);

		mockMvc.perform(get("/employee/personalanswer/1")).andExpect(status().isNoContent());
	}
	
	@Test
	public void getPesonalAnswersBadRequest() throws Exception {
		// Request with invalid PathParameter
		mockMvc.perform(get("/employee/personalanswer/thisisnotastring")).andExpect(status().isBadRequest());
		mockMvc.perform(get("/employee/personalanswer/null")).andExpect(status().isBadRequest());
		mockMvc.perform(get("/employee/personalanswer/\"test")).andExpect(status().isBadRequest());
	}
	
	@Test
	public void findByloginSuccess() throws Exception{
		
		final Employee employee = new Employee();
		
		employee.setLogin("username");
		employee.setId(1);
		
		Mockito.when(employeeRepo.findBylogin(anyString())).thenReturn(employee);
		
		final String response = mockMvc.perform(get("/getEmployeeByUser/username")).andExpect(status().isOk()).andReturn()
				.getResponse().getContentAsString();
		assertThat(response).isNotNull();
		assertThat(response).isNotEmpty();
		assertThat(response).contains("1");
	}
	
	public void findByloginNotFound() throws Exception{
		Mockito.when(employeeRepo.findBylogin(anyString())).thenReturn(null);

		mockMvc.perform(get("/getEmployeeByUser/username")).andExpect(status().isNoContent());
	}
	
}
	
