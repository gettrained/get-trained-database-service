package GET_Trained;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import GET_Trained.domain.Employee;
import GET_Trained.domain.GmdTutorial;
import GET_Trained.domain.GodAnswer;
import GET_Trained.domain.GodPersonalanswer;
import GET_Trained.domain.GodPersonaltutorial;
import GET_Trained.domain.GodQuestion;
import GET_Trained.domain.GodTutorial;
import GET_Trained.domain.GodTutorialtype;
import GET_Trained.repositories.EmployeeRepo;
import GET_Trained.repositories.GmdTutorialRepo;
import GET_Trained.repositories.GodAnswerRepo;
import GET_Trained.repositories.GodPersonalanswerRepo;
import GET_Trained.repositories.GodPersonaltutorialRepo;
import GET_Trained.repositories.GodQuestionRepo;
import GET_Trained.repositories.GodTutorialRepo;
import GET_Trained.repositories.GodTutorialtypeRepo;
import GET_Trained.restControllers.TutorialRestController;

@EnableAutoConfiguration
@ComponentScan
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { TutorialRestController.class })
@SpringBootTest
@AutoConfigureMockMvc
@WebAppConfiguration
@ActiveProfiles("unittest")
public class RepoTests {

	@Autowired
	private GmdTutorialRepo gmdTutorialRepo;
	@Autowired
	private GodTutorialRepo godTutorialRepo;
	@Autowired
	private GodPersonalanswerRepo GodPersonalanswerRepo;
	@Autowired
	private GodAnswerRepo godAnswerRepo;
	@Autowired
	private GodQuestionRepo godQuestionRepo;
	@Autowired
	private GodPersonaltutorialRepo godPersonaltutorialRepo;
	@Autowired
	private EmployeeRepo employeeRepo;
	@Autowired
	private GodTutorialtypeRepo godTutorialtypeRepo;

	@Test
	public void initialization() throws Exception {
		assertThat(gmdTutorialRepo).isNotNull();
		assertThat(godTutorialRepo).isNotNull();
		assertThat(GodPersonalanswerRepo).isNotNull();
		assertThat(godAnswerRepo).isNotNull();
		assertThat(godQuestionRepo).isNotNull();
		assertThat(godPersonaltutorialRepo).isNotNull();
		assertThat(employeeRepo).isNotNull();

	}

	@Test
	public void gmdTutorialRepoFindByIDCodeSuccess() throws Exception {
		final GmdTutorial gmdTutorial = new GmdTutorial();
		gmdTutorial.setIdentificationcode(1);
		gmdTutorial.setTutorialname("MockTut");
		gmdTutorialRepo.saveAndFlush(gmdTutorial);

		GmdTutorial resultgmdTutorial = gmdTutorialRepo.findByIDCode(1);
		assertEquals("MockTut", resultgmdTutorial.getTutorialname());

	}

	@Test
	public void gmdTutorialRepoFindByIDCodeNotFound() throws Exception {
		final GmdTutorial resultgmdTutorial = gmdTutorialRepo.findByIDCode(2);
		assertEquals(null, resultgmdTutorial);
	}

	@Test
	public void godTutorialRepoFindByIDCodeSuccess() throws Exception {
		final GodTutorial godTutorial = new GodTutorial();
		godTutorial.setIdentificationcode(1);
		godTutorial.setTutorialname("MockTut");
		godTutorialRepo.saveAndFlush(godTutorial);

		GodTutorial resultgodTutorial = godTutorialRepo.findByIDCode(1);
		assertEquals("MockTut", resultgodTutorial.getTutorialname());
	}

	@Test
	public void godTutorialRepoFindByIDCodeNotFound() throws Exception {
		final GodTutorial resultgodTutorial = godTutorialRepo.findByIDCode(2);
		assertEquals(null, resultgodTutorial);
	}

	@Test
	public void godPersonalanswerRepofindByPerstutandAnswerIDSuccess() throws Exception {

		final Employee savedEmployee = employeeRepo.saveAndFlush(new Employee());

		final GodTutorial savedgodTutorial = godTutorialRepo.saveAndFlush(new GodTutorial());

		final GodPersonaltutorial godPersonaltutorial = new GodPersonaltutorial();
		godPersonaltutorial.setId(1);
		godPersonaltutorial.setEmployee(savedEmployee);
		godPersonaltutorial.setGodTutorial(savedgodTutorial);
		final GodPersonaltutorial savedgodPersonaltutorial = godPersonaltutorialRepo.saveAndFlush(godPersonaltutorial);

		final GodQuestion godQuestion = new GodQuestion();
		godQuestion.setGodTutorial(savedgodTutorial);
		final GodQuestion savedgodQuestion = godQuestionRepo.saveAndFlush(godQuestion);

		final GodAnswer godAnswer = new GodAnswer();
		godAnswer.setGodQuestion(savedgodQuestion);
		final GodAnswer savedgodAnswer = godAnswerRepo.saveAndFlush(godAnswer);

		final GodPersonalanswer godPersonalanswer = new GodPersonalanswer();

		godPersonalanswer.setGodPersonaltutorial(savedgodPersonaltutorial);
		godPersonalanswer.setGodAnswer(savedgodAnswer);
		godPersonalanswer.setSelected(true);
		GodPersonalanswerRepo.saveAndFlush(godPersonalanswer);

		GodPersonalanswer resultgodPersonalanswer = GodPersonalanswerRepo
				.findByPerstutandAnswerID(savedgodAnswer.getId(), savedgodPersonaltutorial.getId());
		assertEquals(godPersonalanswer.isSelected(), resultgodPersonalanswer.isSelected());
	}

	@Test
	public void GodPersonalanswerRepofindByPerstutandAnswerIDNotFound() throws Exception {
		GodPersonalanswer resultgodPersonalanswer = GodPersonalanswerRepo.findByPerstutandAnswerID(2, 2);
		assertEquals(null, resultgodPersonalanswer);
	}

	@Test
	public void godPersonalanswerRepofindByPerstutandAnswerIDNotFound() throws Exception {
		final GodPersonalanswer resultgodPersonalanswer = GodPersonalanswerRepo.findByPerstutandAnswerID(2, 2);
		assertEquals(null, resultgodPersonalanswer);
	}

	@Test
	public void godTutorialtypeRepofindBytypeSuccess() throws Exception {
		final GodTutorialtype savedgodTutorialtype = godTutorialtypeRepo.saveAndFlush(new GodTutorialtype("image"));

		GodTutorialtype resultgodTutorialtype = godTutorialtypeRepo.findBytype("image");
		assertEquals("image", resultgodTutorialtype.getType());

	}

	@Test
	public void godTutorialtypeRepofindBytypeNotFound() throws Exception {
		final GodTutorialtype godTutorialtype = godTutorialtypeRepo.findBytype("definitelyNOTaType");

		assertEquals(null, godTutorialtype);
	}
	
	@Test
	public void findByloginNotFound() throws Exception {
		final Employee employee = employeeRepo.findBylogin("definitelyNOTaUsernamethanisvialbeforanybody___432146");

		assertEquals(null, employee);
	}
	
	@Test
	public void findByloginSuccess() throws Exception {
		 Employee employee = new Employee();
		 employee.setLogin("username");
		 employeeRepo.saveAndFlush(employee);
		  
		 Employee resultemployee = employeeRepo.findBylogin("username");
		 assertEquals("username", resultemployee.getLogin());

	}

}
